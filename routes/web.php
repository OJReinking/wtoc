<?php
// Startseite
Route::get('/', 'SpielController@about');
// Startseite
Route::get('/about', 'SpielController@about');
// Aktionsseite
Route::get('/aktion', 'SpielController@aktion');
// Button Steuerungsdaten ermitteln
Route::post('/wtoc.steuerungsdaten', ['as' => 'WTOC_Steuerungsdaten_ermitteln', 'uses' => 'SpielController@steuerungsdaten_ermitteln']);
// Button Aktion starten
Route::post('/wtoc.aktion', ['as' => 'WTOC_Aktion_starten', 'uses' => 'SpielController@aktion_starten']);
// Button Fakenamen
Route::post('/wtoc.fakenamen', ['as' => 'WTOC_Fakenamen_generieren', 'uses' => 'SpielController@fakenamen_generieren']);
// Neuwseite
Route::get('/news', 'SpielController@news');
// Liste der Ligen
Route::get('/country', ['as' => 'Country', 'uses' => 'LigenController@country']);
// Liga
Route::get('/ligen/{liganr}', ['as' => 'Liga', 'uses' => 'LigenController@liga']);
// Liga Saison
Route::get('/saison/{liganr}/{saisonnr}', ['as' => 'Saison', 'uses' => 'LigenController@saison']);
// Liga Saison Spieltag
Route::get('/spieltag/{liganr}/{saisonnr}/{spieltagnr}', ['as' => 'Saison', 'uses' => 'LigenController@spieltag']);
// Liga Team
Route::get('/team/{liganr}/{teamnr}', ['as' => 'Team', 'uses' => 'LigenController@team']);
// Liga Team Saison
Route::get('/team_saison/{liganr}/{teamnr}/{saisonnr}', ['as' => 'Team-Saison', 'uses' => 'LigenController@team_saison']);
// Weltmeisterschaft
Route::get('/worldchampionship', ['as' => 'World-Championship', 'uses' => 'LigenController@worldchampionship']);
// Weltmeisterschaft Saison
Route::get('/worldchampionship_saison/{saisonnr}', ['as' => 'World-Championship-Saison', 'uses' => 'LigenController@worldchampionship_saison']);
// Pokalwettbewerb
Route::get('/cup', ['as' => 'Pokal', 'uses' => 'LigenController@cup']);
// Pokalwettbewerb Saison
Route::get('/cup_saison/{saisonnr}', ['as' => 'Pokalsaison', 'uses' => 'LigenController@cup_saison']);
