@extends('layouts.mainlayout')
@section('content')
<section id="section_content">
  <div class="container light-bg">

    <div class="row">
      <div class="col-lg-12">
        <h1>Ligenübersicht</h1>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12">
        @foreach ($ligen as $single_liga)
          <a href='ligen/{{ $single_liga->LandNr }}'>{{ $single_liga->LandName }}</a><br />
        @endforeach
      </div>
    </div>

  </div>
</section>

@stop
