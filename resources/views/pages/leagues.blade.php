@extends('layouts.mainlayout')
@section('content')

<section id="section_content">
  <div class="container light-bg">
    <div class="row">
      <div class="col-lg-12">
        <h1>{!! $liganame !!}</h1>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-6">
        {!! $saisonliste_url !!}
      </div>
      <div class="col-lg-6">
        {!! $teamliste_url !!}
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12">
        <hr />
      </div>
    </div>

  </div>
</section>

@stop
