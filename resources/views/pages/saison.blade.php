@extends('layouts.mainlayout')
@section('content')

<section id="section_content">
  <div class="container light-bg">

    <div class="row">
      <div class="col-lg-12">
        <h1>{{ $saisonname }} {{ $liganame }}</h1>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12">
        <div class="btn-group-vertical" role="group">
          {!! $spieltagliste_url !!}
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12">
        <hr />
      </div>
    </div>
  </div>
</section>

@stop
