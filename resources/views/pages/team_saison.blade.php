@extends('layouts.mainlayout')
@section('content')

<section id="section_content">
  <div class="container light-bg">

    <div class="row">
      <div class="col-lg-12">
        <h1>{{ $teamname }}</h1>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12">
        <div id="accordion_A">
          <div class="card">
            @foreach ($saisondaten as $single_saison)
              <div class="card-header" id="HeaderSaison{{ $single_saison['saisonnr'] }}">
                <h5 class="mb-0">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#ContentSaison{{ $single_saison['saisonnr'] }}" aria-expanded="true" aria-controls="ContentSaison{{ $single_saison['saisonnr'] }}">
                    {{ $single_saison['saisonnr'] }}. Saison
                  </button>
                </h5>
                <div id="ContentSaison{{ $single_saison['saisonnr'] }}" class="collapse" aria-labelledby="HeaderSaison{{ $single_saison['saisonnr'] }}" data-parent="#accordion_A">
                  <div class="card-body">
                    <h3>Team</h3>
                    {!! $teaminfo !!}
                    <h3>Mannschaft</h3>
                    {!! $mannschaft !!}
                    <h3>Spielergebnisse</h3>
                    {!! $spielliste !!}
                    <h3>Torjäger (Liga)</h3>
                    {!! $single_saison['ligatorjägerliste'] !!}
                    <h3>Tabelle</h3>
                    {!! $single_saison['saisontabelle'] !!}
                    <h3>Liga-Heimspiele</h3>
                    {!! $single_saison['ligaheimspiele'] !!}
                    <h3>Liga-Auswärtsspiele</h3>
                    {!! $single_saison['ligagastspiele'] !!}
                    <h3>Pokal-Heimspiele</h3>
                    {!! $single_saison['pokalheimspiele'] !!}
                    <h3>Pokal-Auswärtsspiele</h3>
                    {!! $single_saison['pokalgastspiele'] !!}
                    <h3>Torjäger (Pokal)</h3>
                    {!! $single_saison['pokaltorjägerliste'] !!}
                  </div>
                </div>
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12">
        <hr />
      </div>
    </div>

  </div>

</section>

@stop
