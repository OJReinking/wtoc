@extends('layouts.mainlayout')
@section('content')

<section id="section_content">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h2>Befehle</h2>
        {!! Form::open(['route' => 'WTOC_Steuerungsdaten_ermitteln', 'class' => 'form-horizontal']) !!}
        <div class="form-group">
          <div class="col-sm-8">
            {!! Form::submit('Steuerungsdaten ermitteln', ['class' => 'btn btn-primary btn-block']) !!}
          </div>
        </div>
        {!! Form::close() !!}

        {!! Form::open(['route' => 'WTOC_Aktion_starten', 'class' => 'form-horizontal']) !!}
        <div class="form-group">
          <div class="col-sm-8">
            {!! Form::submit('Aktion starten', ['class' => 'btn btn-primary btn-block']) !!}
          </div>
        </div>
        {!! Form::close() !!}

        {!! Form::open(['route' => 'WTOC_Fakenamen_generieren', 'class' => 'form-horizontal']) !!}
        <div class="form-group">
          <div class="col-sm-8">
            {!! Form::submit('Fakenamen generieren', ['class' => 'btn btn-primary btn-block']) !!}
          </div>
        </div>
        {!! Form::close() !!}
      </div>
    </div>

    @if ($fehlerkz)
      <div class="row">
        <div class="col-lg-12">
          <div class="alert alert-danger">
            <h2>Fehler</h2>
              {!! $fehlermeldung !!}<br />
          </div>
        </div>
    </div>
    @endif

    @if ($steuerung_kz)
      <div class="row">
        <div class="col-lg-12">
          <div class="alert alert-success" role="alert">
            <h2 class="page-header">Steuerungsdaten</h2>
            <div>
              {!! $steuerungsbericht !!}
            </div>
            <hr />
          </div>
        </div>
      </div>
    @endif

    @if ($aktion_kz)
      <div class="row">
        <div class="col-lg-12">
          <div class="alert alert-success" role="alert">
            <h2 class="page-header">Aktionsbericht</h2>
            <div>
              {!! $aktionsbericht !!}
            </div>
            <hr />
          </div>
        </div>
      </div>
    @endif

  </div>
</section>
@stop
