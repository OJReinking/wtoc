@extends('layouts.mainlayout')
@section('content')

<section id="section_content">
  <div class="container light-bg">

    <div class="row">
      <div class="col-lg-12">
        <h1>World-Championship - {{$saisonnr}}. Saison</h1>
      </div>
    </div>

    <div id="accordion_A">
      <div class="card">
        @foreach ($pokaldaten as $single_pokal)
          <div class="card-header" id="HeaderPokal{{ $single_pokal['saisonnr'] }}">
            <h5 class="mb-0">
              <button class="btn btn-link" data-toggle="collapse" data-target="#ContentPokal{{ $single_pokal['saisonnr'] }}" aria-expanded="true" aria-controls="ContentPokal{{ $single_pokal['saisonnr'] }}">
                {{ $single_pokal['saisonnr'] }}. Saison
              </button>
            </h5>
            <div id="ContentPokal{{ $single_pokal['saisonnr'] }}" class="collapse" aria-labelledby="HeaderPokal{{ $single_pokal['saisonnr'] }}" data-parent="#accordion_A">
              <div class="card-body">
                <h3>World-Championship</h3>
                {!! $single_pokal['pokalrunden'] !!}
                <h3>WM-Torjägerliste</h3>
                {!! $single_pokal['wmtorjägerliste'] !!}
                <h3>Welttorjägerliste</h3>
                {!! $single_pokal['welttorjägerliste'] !!}
                <h3>Teameinnahmen-Gesamt</h3>
                {!! $single_pokal['teameinnahmen_gesamt'] !!}
                <h3>Teameinnahmen-Einnahmetyp</h3>
                {!! $single_pokal['teameinnahmen'] !!}
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12">
        <hr />
      </div>
    </div>

  </div>
</section>

@stop
