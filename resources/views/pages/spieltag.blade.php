@extends('layouts.mainlayout')
@section('content')

<section id="section_content">
  <div class="container light-bg">

    <div class="row">
      <div class="col-lg-12">
        <h1>{{ $saisonname }} {{ $liganame }} - Spieltag: {{ $spieltag }}</h1>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12">
        <h3>Paarungen</h3>
        {!! $spielpaarungen !!}
        <h3>Tabelle</h3>
        {!! $spieltagtabelle !!}
        <h3>Torjäger</h3>
        {!! $torjägerliste !!}
        <h3>Spielberichte</h3>
        {!! $spielberichte !!}
        <hr />
        <div class="form-group">
          {{ Form::label('spielpaarung', 'Spielpaarungen') }}
          {{ Form::textArea('spielpaarung', $saisonnr . '. Saison  - ' . $spieltag . '. Spieltag - ' . $liganame . '<br />' . $spielpaarungen , ['class'=> 'form-control']) }}
        </div>
        <div class="form-group">
          {{ Form::label('spielberichte', 'Spielberichte') }}
          {{ Form::textArea('spielberichte', $spielberichte , ['class'=> 'form-control']) }}
        </div>
        <div class="form-group">
          {{ Form::label('spieltagtabelle', 'Spieltagtabelle') }}
          {{ Form::textArea('spieltagtabelle', $spieltagtabelle , ['class'=> 'form-control']) }}
        </div>
      </div>
    </div>

  </div>
</section>

@stop
