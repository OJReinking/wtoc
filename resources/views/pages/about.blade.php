@extends('layouts.mainlayout')
@section('content')

<!-- Header -->
<header>
  <div class="container">
    <div class="slider-container">
      <div class="intro-text">
        <div class="intro-lead-in">Willkommen in der WTOC-Welt</div>
        <div class="intro-heading">2.816 Spieler<br />256 Teams<br/>16 Länder<br/>1 Sieger</div>
        <a href="#about" class="page-scroll btn btn-xl">Erfahren Sie hier mehr...</a>
      </div>
    </div>
  </div>
</header>
<section id="about" class="light-bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <div class="section-title">
          <h2>WTOC</h2>
          <p>World Teamplayer Organization Championship</p>
        </div>
      </div>
    </div>
    <div class="row">
      <!-- about module -->
      <div class="col-md-3 text-center">
        <div class="mz-module-about">
          <i class="fa fa-code ot-circle"></i>
          <h3>Spielregeln</h3>
          <p><a a href="#spielregeln">Eine ausführliche Beschreibung der WTOC-Spielregeln</a></p>
        </div>
      </div>
      <!-- end about module -->
      <!-- about module -->
      <div class="col-md-3 text-center">
        <div class="mz-module-about">
          <i class="fa fa-users ot-circle"></i>
          <h3>Teilnehmer</h3>
          <p>Insgesamt nehmen 256 Städte am WTOC teil.</p>
          <p><a href="#teams">Alle Teams im Überblick</a></p>
        </div>
      </div>
      <!-- end about module -->
      <!-- about module -->
      <div class="col-md-3 text-center">
        <div class="mz-module-about">
          <i class="fa fa-book ot-circle"></i>
          <h3>WTOC-Buch</h3>
          <p><a href="#buch">Tauchen Sie jetzt ein in die WTOC-Welt - alle WTOC-Bücher</a></p>
        </div>
      </div>
      <!-- end about module -->
      <!-- about module -->
      <div class="col-md-3 text-center">
        <div class="mz-module-about">
          <i class="fa fa-trophy ot-circle"></i>
          <h3>Siegerliste</h3>
          <p><a href="#siegerliste">Hier finden Sie die Liste der bisherigen Weltmeister</a></p>
        </div>
      </div>
      <!-- end about module -->
    </div>
  </div>
  <!-- /.container -->
</section>
<section class="overlay-dark bg-img1 dark-bg short-section">
  <div class="container text-center">
    <div class="row">
      <div class="col-md-3 mb-sm-30">
        <div class="counter-item">
          <h2 data-count="256">256</h2>
          <h6>Teams</h6>
        </div>
      </div>
      <div class="col-md-3 mb-sm-30">
        <div class="counter-item">
          <h2 data-count="16">16</h2>
          <h6>Länder</h6>
        </div>
      </div>
      <div class="col-md-3 mb-sm-30">
        <div class="counter-item">
          <h2 data-count="2816">2816</h2>
          <h6>Spieler</h6>
        </div>
      </div>
      <div class="col-md-3 mb-sm-30">
        <div class="counter-item">
          <h2 data-count="1">1</h2>
          <h6>Sieger</h6>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="spielregeln" class="light-bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <div class="section-title">
          <h2>Spielregeln</h2>
          <p>Die WTOC-Spielregeln</p>
        </div>
      </div>
    </div>
    <div class="row">
      <!-- start portfolio item -->
      <div class="col-md-4">
        <div class="ot-portfolio-item">
          <figure class="effect-bubba">
            <figcaption>
              <h2>Landesmeisterschaft</h2>
              <p>Werde Landesmeister</p>
              <a href="#" data-toggle="modal" data-target="#modal_landesmeister">weitere Infos</a>
            </figcaption>
          </figure>
        </div>
      </div>
      <!-- end portfolio item -->
      <!-- start portfolio item -->
      <div class="col-md-4">
        <div class="ot-portfolio-item">
          <figure class="effect-bubba">
            <figcaption>
              <h2>WTC-Cup</h2>
              <p>Hol das Ding</p>
              <a href="#" data-toggle="modal" data-target="#modal_cup">weitere Infos</a>
            </figcaption>
          </figure>
        </div>
      </div>
      <!-- end portfolio item -->
      <!-- start portfolio item -->
      <div class="col-md-4">
        <div class="ot-portfolio-item">
          <figure class="effect-bubba">
            <figcaption>
              <h2>Weltmeisterschaft</h2>
              <p>1 Sieger</p>
              <a href="#" data-toggle="modal" data-target="#modal_wm">weitere Infos</a>
            </figcaption>
          </figure>
        </div>
      </div>
      <!-- end portfolio item -->
    </div>
  </div><!-- end container -->
</section>
<section id="teams">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <div class="section-title">
          <h2>Unsere Teams</h2>
          <p>Aus 16 Ländern gehören insgesamt 256 Teams zur WTOC-Familie.</p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 text-center">
        <div class="owl-carousel">
          <div class="item1">
            <div class="partner-logo"><b>Berlin</b><br />Deutschland</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>London</b><br />Großbritanien</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>Rio de Janeiro</b><br />Brasilien</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>Atlanta</b><br />USA</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>Lille</b><br />Frankreich</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>Liverpool</b><br />Großbritanien</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>München</b><br />Deutschland</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>Paris</b><br />Frankreich</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>Dortmund</b><br />Deutschland</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>Machester</b><br />Großbritanien</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>Marseille</b><br />Frankreich</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>Madrid</b><br />Spanien</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>Berlin</b><br />Deutschland</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>Rotterdam</b><br />Niederlande</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>Kopenhagen</b><br />Dänemark</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>Nkoteng</b><br />Kamerun</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>Buenos Aires</b><br />Argentinien</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>Washington</b><br />USA</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>Zapopan</b><br />Mexiko</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>Angol</b><br />Chile</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>Mexiko-Stadt</b><br />Mexiko</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>Montreal</b><br />Kanada</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>Puente Alto</b><br />Chile</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>London</b><br />Großbritanien</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>Zweibrücken</b><br />Deutschland</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>Belleville</b><br />Kanada</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>Hermosillo</b><br />Mexiko</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>Tuscon</b><br />USA</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>Berlin</b><br />Deutschland</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>Phoenix</b><br />USA</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>Berlin</b><br />Deutschland</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>Sydney</b><br />Australien</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>Wellington</b><br />Neuseeland</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>Santa Fe</b><br />Argentinien</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>Curitiba><br />Brasilien</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>Bafoussam</b><br />Kamerun</div>
          </div>
          <div class="item">
            <div class="partner-logo"><b>Odense</b><br />Dänemark</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="buch" class="light-bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <div class="section-title">
          <h2>Bücher</h2>
          <p>In unseren WTOC-Büchern finden Sie alle Informationen rund um die teilnehmenden 256 Teams. Viel Spaß bei der Lektüre.</p>
        </div>
      </div>
    </div>
    <div class="row">
      <!-- team member item -->
      <div class="col-md-6">
        <div class="team-item">
          <div class="team-text">
            <h3>Die Ruhe vor dem Sturm</h3>
            <div class="team-location">1. Saison</div>
            <div class="team-position">– Oliver Nigbur –</div>
            <p>Oliver Nigbur, die Nummer 9 der Spvg. Zweibrücken</p>
          </div>
        </div>
      </div>
      <!-- end team member item -->
      <!-- team member item -->
      <div class="col-md-6">
        <div class="team-item">
          <div class="team-text">
            <h3>Erntezeit</h3>
            <div class="team-location">2. Saision</div>
            <div class="team-position">– Oliver Nigbur –</div>
            <p>Oliver Nigbur, die Nummer 9 der Spvg. Zweibrücken</p>
          </div>
        </div>
      </div>
      <!-- end team member item -->
    </div>
  </div>
</section>
<section id="siegerliste">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <div class="section-title">
          <h2>Weltmeister</h2>
          <p>1. Saison: Zweibrücker Löwen für Deutschland</p>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="contact">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <div class="section-title">
          <h2>Kontaktieren Sie uns</h2>
          <p>Sollten Sie eine Frage haben, wir antworten! Versprochen!</p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3">
        <h3>WTOC</h3>
        <p>410 Jockey Hollow Street, Charlotte, NC 28205</p>
        <p><i class="fa fa-phone"></i> 443-585-03545</p>
        <p><i class="fa fa-envelope"></i> mail@wtoc.com</p>
      </div>
      <div class="col-md-6">
        <form name="sentMessage" id="contactForm" novalidate="">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Your Name *" id="name" required="" data-validation-required-message="Please enter your name.">
                <p class="help-block text-danger"></p>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <input type="email" class="form-control" placeholder="Your Email *" id="email" required="" data-validation-required-message="Please enter your email address.">
                <p class="help-block text-danger"></p>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <textarea class="form-control" placeholder="Your Message *" id="message" required="" data-validation-required-message="Please enter a message."></textarea>
                <p class="help-block text-danger"></p>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="row">
            <div class="col-lg-12 text-center">
              <div id="success"></div>
              <button type="submit" class="btn">Send Message</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>

<div class="modal" tabindex="-1" role="dialog" id="modal_landesmeister">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">WTC-Landesmeister</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>In jedem der 16 teilnehmenden Länder findet die Landesmeisterschaft statt.</p>
        <p>Modus: Jeder gegen jeden mit Hin- und Rückspiel.</p>
        <p>D.h. es gibt in jeder Saison 30 Spieltage.</p>
        <p>Der Sieger erhält eine Prämie in Höhe von 1.000.000 TC.</p>
        <p>Für jeden Punkt erhält ein Team 20.000 TC.</p>
        <p>Insgesamt werden 8 Ko.-Runden gespielt.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="modal_cup">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">WTC-Cup</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>An diesem Wettbewerb nehmen alle 256 Teams teil.</p>
        <p>Modus: K.O. Runde mit Hin- und Rückspiel</p>
        <p>Der Sieger erhält eine Prämie in Höhe von 1.000.000 TC.</p>
        <p>Für jeden Punkt erhält ein Team 40.000 TC.</p>
        <p>Insgesamt werden 8 Ko.-Runden gespielt.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="modal_wm">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">WTC-Championship</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>An diesem Wettbewerb nehmen alle 16 Landesmeister teil.</p>
        <p>Modus: K.O. Runde mit Hin- und Rückspiel</p>
        <p>Der Sieger erhält eine Prämie in Höhe von 3.000.000 TC.</p>
        <p>Für jeden Punkt erhält ein Team 60.000 TC.</p>
        <p>Insgesamt werden 8 Ko.-Runden gespielt.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
      </div>
    </div>
  </div>
</div>


@stop
