@extends('layouts.mainlayout')
@section('content')
<section id="section_content">
  <div class="container light-bg">
    <div class="row">
      <div class="col-lg-12">
        <h1>Cup</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-3">
        <div class="btn-group-vertical" role="group">
          {!! $saisonliste_url !!}
        </div>
      </div>
      <div class="col-lg-9">
        {!! $endspielliste !!}
      </div>
    </div>
  </div>
</section>
@stop
