@extends('layouts.mainlayout')
@section('content')

<section id="section_content">
  <div class="container light-bg">

    <div class="row">
      <div class="col-lg-12">
        <h1>{{ $teamname }}</h1>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-3">
        <div class="btn-group-vertical" role="group">
          <h3>Saison</h3>
          {!! $saisonliste_url !!}
        </div>
      </div>
      <div class="col-lg-9">
        <h3>Platzierungen</h3>
        {!! $platzierungen !!}
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12">
        <h3>Spiele</h3>
        {!! $spielliste !!}
      </div>
    </div>

  </div>
</section>

@stop
