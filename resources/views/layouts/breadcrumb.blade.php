<!-- Breadcrumb -->
<section id="section_breadcrumb">
    <div aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/about">Home</a></li>
        @if ($seitenname == 'aktion')
          <li class="breadcrumb-item active" aria-current="page">Aktion</li>
        @endif
        @if ($seitenname == 'news')
          <li class="breadcrumb-item active" aria-current="page">News</li>
        @endif
        @if ($seitenname == 'saison')
          <li class="breadcrumb-item"><a href="/country">Ligen</a></li>
          <li class="breadcrumb-item"><a href="/ligen/{{$liganr}}">{!! $liganame !!}</a></li>
          <li class="breadcrumb-item active" aria-current="page">{!! $saisonname !!}</li>
        @endif
        @if ($seitenname == 'spieltag')
          <li class="breadcrumb-item"><a href="/country">Ligen</a></li>
          <li class="breadcrumb-item"><a href="/ligen/{{$liganr}}">{!! $liganame !!}</a></li>
          <li class="breadcrumb-item"><a href="/saison/{{$liganr}}/{{$saisonnr}}">{!! $saisonname !!}</a></li>
          <li class="breadcrumb-item active" aria-current="page">{!! $spieltag !!}. Spieltag</li>
        @endif
        @if ($seitenname == 'team')
          <li class="breadcrumb-item"><a href="/country">Ligen</a></li>
          <li class="breadcrumb-item"><a href="/ligen/{{$liganr}}">{!! $liganame !!}</a></li>
          <li class="breadcrumb-item active" aria-current="page">{!! $teamname !!}</li>
        @endif
        @if ($seitenname == 'team_saison')
          <li class="breadcrumb-item"><a href="/country">Ligen</a></li>
          <li class="breadcrumb-item"><a href="/ligen/{{$liganr}}">{!! $liganame !!}</a></li>
          <li class="breadcrumb-item active"><a href="/team/{{$liganr}}/{{$teamnr}}">{!! $teamname !!}</a></li>
          <li class="breadcrumb-item active" aria-current="page">{!! $saisonnr !!}. Saison</li>
        @endif
        @if ($seitenname == 'country')
          <li class="breadcrumb-item active" aria-current="page"><a href="/country">Ligen</a></li>
        @endif
        @if ($seitenname == 'liga')
          <li class="breadcrumb-item"><a href="/country">Ligen</a></li>
          <li class="breadcrumb-item active" aria-current="page">{!! $liganame !!}</li>
        @endif
        @if ($seitenname == 'cup')
          <li class="breadcrumb-item active" aria-current="page">Pokal</li>
        @endif
        @if ($seitenname == 'cup_saison')
          <li class="breadcrumb-item"><a href="/cup">Pokal</a></li>
          <li class="breadcrumb-item active" aria-current="page">{!! $saisonnr !!}. Saison</li>
        @endif
        @if ($seitenname == 'worldchampionship')
          <li class="breadcrumb-item active" aria-current="page">World Championship</li>
        @endif
        @if ($seitenname == 'worldchampionship_saison')
          <li class="breadcrumb-item"><a href="/worldchampionship">World Championship</a></li>
          <li class="breadcrumb-item active" aria-current="page">{!! $saisonnr !!}. Saison</li>
        @endif
      </ol>
    </div>
</section>
