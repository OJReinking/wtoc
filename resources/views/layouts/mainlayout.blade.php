<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="World Teamplayer Organization Championship">
    <meta name="author" content="Oliver Reinking">
    <link rel="icon" href="/favicon.ico">
    <title>WTOC</title>
    <link href="/css/app.css" rel="stylesheet">
    <!-- Start Lattes-Design -->
    <!-- Bootstrap core CSS -->
		<link href="/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
		<!-- Custom styles for this template -->
		<link href="/css/owl.carousel.css" rel="stylesheet">
		<link href="/css/owl.theme.default.min.css"  rel="stylesheet">
		<link href="/css/style.css" rel="stylesheet">
		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<script src="/js/ie-emulation-modes-warning.js"></script>
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

</head>

<body id="page-top">

  <nav id="wtoc_navigation" class="navbar navbar-default fixed-top navbar-expand-lg">
    <div class="container">
      <a class="navbar-brand" href="/about">
        <img src="/images/Logo_WTOC.png" title="WTOC" class="wtoc_header_logo"></img>
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#wtoc_navbar" aria-controls="wtoc_navbar" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="wtoc_navbar" >
        <div class="navbar-nav mr-auto">
          <div class="nav-item">
            <a class="nav-link" href="/about">Home</a>
          </div>
          <div class="nav-item">
            <a class="nav-link" href="/aktion">Aktion</a>
          </div>
          <div class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLigen" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Ligen
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLigen">
              <a class="dropdown-item" href="/country">Ligenübersicht</a>
              <hr />
              <a class="dropdown-item" href="/ligen/1">Deutschand</a>
              <a class="dropdown-item" href="/ligen/2">Italien</a>
              <a class="dropdown-item" href="/ligen/3">Frankreich</a>
              <a class="dropdown-item" href="/ligen/4">England</a>
              <a class="dropdown-item" href="/ligen/5">Spanien</a>
              <a class="dropdown-item" href="/ligen/6">Niederlande</a>
              <a class="dropdown-item" href="/ligen/7">Dänemark</a>
              <a class="dropdown-item" href="/ligen/8">Kamerun</a>
              <a class="dropdown-item" href="/ligen/9">Brasilien</a>
              <a class="dropdown-item" href="/ligen/10">Argentinien</a>
              <a class="dropdown-item" href="/ligen/11">Australien</a>
              <a class="dropdown-item" href="/ligen/12">Neuseeland</a>
              <a class="dropdown-item" href="/ligen/13">USA</a>
              <a class="dropdown-item" href="/ligen/14">Mexiko</a>
              <a class="dropdown-item" href="/ligen/15">Kanada</a>
              <a class="dropdown-item" href="/ligen/16">Chile</a>
            </div>
          </div>
          <div class="nav-item">
            <a class="nav-link"  href="/cup">Cup</a>
          </div>
          <div class="nav-item">
            <a class="nav-link"  href="/worldchampionship">World Championship</a>
          </div>
          <div class="nav-item">
            <a class="nav-link" href="/news">News</a>
          </div>
        </div>
      </div>
    </div>
  </nav>

  <div>
    @include('layouts.breadcrumb')
    @yield('content')
  </div>

  <p id="back-top">
    <a href="#top"><i class="fa fa-angle-up"></i></a>
  </p>

  <nav class="navbar_footer_wtoc">
    <div class="container">
      <div class="row">
        <div class="col-sm d-none d-sm-block text-left">
          <img src="/images/Logo_WTOC.png" title="WTOC" class="wtoc_footer_logo"></img></a>
        </div>
        <div class="col-sm text_footer_wtoc text-nowrap text-right">
          World Teamplayer Organization Championship<br />
          Copyright © Oliver Nigbur ({!! Config::get('konstanten.wtoc_aktuellesjahr') !!})<br />
          Version: {!! Config::get('konstanten.wtoc_version') !!}<br />
          {{ermittle_Datum(Carbon\Carbon::now(), 2)}}<br />
          Designed by <a href="http://moozthemes.com"><span>MOOZ</span>Themes.com</a><br />
        </div>
      </div>
    </div>
   </nav>

  <!--<script src="/js/app.js"></script>-->
  <!-- Lattes-Design -->
  <script src="/js/jquery.min.js"></script>
  <script src="/js/jquery.easing.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/owl.carousel.min.js"></script>
  <script src="/js/cbpAnimatedHeader.js"></script>
  <script src="/js/theme-scripts.js"></script>
  <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
  <script src="/js/ie10-viewport-bug-workaround.js"></script>
  <script>
    $(function () {
      $('[data-toggle="popover"]').popover()
    })
    $('.popover-dismiss').popover({
      trigger: 'focus'
    })
  </script>
</body>
</html>
