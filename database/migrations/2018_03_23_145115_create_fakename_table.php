<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFakenameTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('FakeName', function (Blueprint $table) {
            $table->increments('Nr');
            $table->string('Vorname', 50);
            $table->string('Nachname', 100);
            $table->integer('zugLandNr')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('FakeName');
    }
}
