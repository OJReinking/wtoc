<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\Land;
use App\Saison;
use App\Steuerung;
use App\SpielPlan;
use App\Spiel;
use App\SpielTore;
use App\Team;
use App\TeamKonto;
use App\Player;
use App\LigaTabelle;
use App\PokalTabelle;
use App\WMTabelle;

class LigenController extends Controller
{
  //
  public function worldchampionship()
  {
    // ermittle notwendige Werte aus der Tabelle Steuerung
    $maxSaisonNr = 0;
    //
    $steuerung = Steuerung::where('SNr', '=', 1)
      ->firstOrFail();
    //
    $maxSaisonNr = $steuerung->SzugSaisonNr;
    //
    $saisonliste = Saison::select('SaisonName', 'SaisonNr')
      ->where('SaisonNr', '<=', $maxSaisonNr)
      ->orderBy('SaisonNr')
      ->get();
    //
    $saisonliste_url = '';
    //
    foreach ($saisonliste as $single_saison) {
      $saisonliste_url .= '<a href="/worldchampionship_saison/' . $single_saison->SaisonNr . '" class="btn btn-success" role="button">' . $single_saison->SaisonName . '</a>';
    }
    // endspielliste
    $endspielliste = '<table class="table table-bordered table-striped">';
    $endspielliste .= '<thead class="thead-dark">';
    $endspielliste .= '<tr>';
    $endspielliste .= '<th scope="col">Saison</th>';
    $endspielliste .= '<th scope="col">Team A</th>';
    $endspielliste .= '<th scope="col">Team B</th>';
    $endspielliste .= '<th scope="col">Hinspiel</th>';
    $endspielliste .= '<th scope="col">Rückspiel</th>';
    $endspielliste .= '<th scope="col">Weltmeister</th>';
    $endspielliste .= '</tr>';
    $endspielliste .= '</thead>';
    //
    $final_liste = DB::table('wmtabelle')
      ->select('wmtabelle.*', 'Heim.TeamName as HeimTeam', 'Gast.TeamName as GastTeam', 'Sieger.TeamName as SiegerTeam')
      ->join('team as Heim', 'Heim.TeamNr', '=', 'wmtabelle.WMTeamANr')
      ->join('team as Gast', 'Gast.TeamNr', '=', 'wmtabelle.WMTeamBNr')
      ->join('team as Sieger', 'Sieger.TeamNr', '=', 'wmtabelle.WMTeamSiegerNr')
      ->where('WMRunde', '=', 4)
      ->orderBy('WMzugSaisonNr')
      ->get();
    //
    foreach($final_liste as $single_finale)
    {
      $endspielliste .= '<tr>';
      $endspielliste .= '<td>' . $single_finale->WMzugSaisonNr . '</td>';
      $endspielliste .= '<td>' . $single_finale->HeimTeam . '</td>';
      $endspielliste .= '<td>' . $single_finale->GastTeam . '</td>';
      $endspielliste .= '<td>' . $single_finale->WMTAHTor . ' : ' . $single_finale->WMTBHTor . '</td>';
      $endspielliste .= '<td>' . $single_finale->WMTARTor . ' : ' . $single_finale->WMTBRTor . '</td>';
      $endspielliste .= '<td>' . $single_finale->SiegerTeam . '</td>';
      $endspielliste .= '</tr>';
    }
    $endspielliste .= '</table>';
    // ermittle die Welt-Torjägerliste
    $torjäger = DB::table('spieltore')
      ->select('PlayerName', 'PlayerTrikotNr', 'TeamName', DB::raw('COUNT(SpielToreNr) as Tore'))
      ->join('player', 'player.PlayerNr', '=', 'spieltore.SpielTorezugPlayerNr')
      ->join('team', 'team.TeamNr', '=', 'spieltore.SpielTorezugTeamNr')
      ->groupBy('SpielTorezugPlayerNr', 'PlayerName', 'PlayerTrikotNr', 'TeamName')
      ->where('SpielToreTyp', '=', 0)
      ->limit(50)
      ->orderBy('Tore', 'desc')
      ->orderBy('TeamName')
      ->get();
    //
    $welttorjaegerliste = '';
    $welttorjaegerliste = '<table class="table table-bordered table-striped">';
    //
    $welttorjaegerliste .= '<thead class="thead-dark">';
    $welttorjaegerliste .= '<tr>';
    $welttorjaegerliste .= '<th col"scope">Torschütze</th>';
    $welttorjaegerliste .= '<th col"scope">Verein</th>';
    $welttorjaegerliste .= '<th col"scope">Tore</th>';
    $welttorjaegerliste .= '</thead>';
    foreach($torjäger as $torschütze)
    {
        $welttorjaegerliste .= '<tr>';
        $welttorjaegerliste .= '<td>' . $torschütze->PlayerName . ' ('. $torschütze->PlayerTrikotNr . ')</td>';
        $welttorjaegerliste.= '<td>' . $torschütze->TeamName . '</td>';
        $welttorjaegerliste.= '<td>' . $torschütze->Tore . '</td>';
        $welttorjaegerliste.= '</tr>';
    }
    $welttorjaegerliste.= '</table>';
    // ermittle die Welt-Torjägerliste
    $wmtorjäger = DB::table('spieltore')
      ->select('PlayerName', 'PlayerTrikotNr', 'TeamName', DB::raw('COUNT(SpielToreNr) as Tore'))
      ->join('player', 'player.PlayerNr', '=', 'spieltore.SpielTorezugPlayerNr')
      ->join('team', 'team.TeamNr', '=', 'spieltore.SpielTorezugTeamNr')
      ->groupBy('SpielTorezugPlayerNr', 'PlayerName', 'PlayerTrikotNr', 'TeamName')
      ->where('SpielTorezugSpieltypNr', '>=', 200)
      ->where('SpielToreTyp', '=', 0)
      ->limit(50)
      ->orderBy('Tore', 'desc')
      ->orderBy('TeamName')
      ->get();
    //
    $wmtorjaegerliste = '';
    $wmtorjaegerliste = '<table class="table table-bordered table-striped">';
    //
    $wmtorjaegerliste .= '<thead class="thead-dark">';
    $wmtorjaegerliste .= '<tr>';
    $wmtorjaegerliste .= '<th col"scope">Torschütze</th>';
    $wmtorjaegerliste .= '<th col"scope">Verein</th>';
    $wmtorjaegerliste .= '<th col"scope">Tore</th>';
    $wmtorjaegerliste .= '</thead>';
    foreach($wmtorjäger as $torschütze)
    {
        $wmtorjaegerliste .= '<tr>';
        $wmtorjaegerliste .= '<td>' . $torschütze->PlayerName . ' ('. $torschütze->PlayerTrikotNr . ')</td>';
        $wmtorjaegerliste.= '<td>' . $torschütze->TeamName . '</td>';
        $wmtorjaegerliste.= '<td>' . $torschütze->Tore . '</td>';
        $wmtorjaegerliste.= '</tr>';
    }
    $wmtorjaegerliste.= '</table>';
    // WM-Spiele
    $wmspiele = '<div class="table-responsive-sm">';
    $wmspiele .= '<table class="table table-sm table-bordered table-striped">';
    //
    $wmspiele .= '<thead class="thead-dark">';
    $wmspiele .= '<tr>';
    $wmspiele .= '<th scope="col">Saison</th>';
    $wmspiele .= '<th scope="col">Runde</th>';
    $wmspiele .= '<th scope="col">Hinspiel</th>';
    $wmspiele .= '<th scope="col">Rückspiel</th>';
    $wmspiele .= '<th scope="col">Sieger</th>';
    $wmspiele .= '</tr>';
    $wmspiele .= '</thead>';
    //
    $pokaltabelle = WMTabelle::orderBy('WMNr')
      ->get();
    //
    foreach($pokaltabelle as $spiel)
    {
      $wmspiele .= '<tr>';
      //
      $wmspiele .= '<td>' . $spiel->WMzugSaisonNr . '</td>';
      //
      $wmspiele .= '<td>' . ermittle_Pokalrundenname($spiel->WMRunde, 2) . '</td>';
      // Hinspiel
      $wmspiele .= '<td>';
      if ($spiel->WMHSpNr>0)
      {
        $wmspiele .= ermittle_Teamnamen_Land($spiel->WMTeamANr);
        $wmspiele .= ' ' . $spiel->WMTAHTor . ' : ' . $spiel->WMTBHTor . ' ';
        $wmspiele .= ermittle_Teamnamen_Land($spiel->WMTeamBNr);
      } else {
        $wmspiele .= ermittle_Teamnamen_Land($spiel->WMTeamANr);
        $wmspiele .= ' : ';
        $wmspiele .= ermittle_Teamnamen_Land($spiel->WMTeamBNr);
      }
      $wmspiele .= '</td>';
      // Rückspiel
      $wmspiele .= '<td>';
      if ($spiel->WMRSpNr>0)
      {
        $wmspiele .= ermittle_Teamnamen_Land($spiel->WMTeamBNr);
        $wmspiele .= ' ' . $spiel->WMTBRTor . ' : ' . $spiel->WMTARTor . ' ';
        $wmspiele .= ermittle_Teamnamen_Land($spiel->WMTeamANr);
      } else {
        $wmspiele .= ermittle_Teamnamen_Land($spiel->WMTeamBNr);
        $wmspiele .= ' : ';
        $wmspiele .= ermittle_Teamnamen_Land($spiel->WMTeamANr);
      }
      //Sieger
      $wmspiele .= '<td>';
      if ($spiel->WMTeamSiegerNr>0)
      {
        $wmspiele .= ermittle_Teamnamen_Land($spiel->WMTeamSiegerNr);
      }
      $wmspiele .= '</td>';
      //
      $wmspiele .= '</tr>';
    }
    //
    $wmspiele .= '</table>';
    $wmspiele .= '</div>';
    //
    return view('pages.worldchampionship', [
      'seitenname'          => 'worldchampionship',
      'welttorjaegerliste'  => $welttorjaegerliste,
      'wmtorjaegerliste'    => $wmtorjaegerliste,
      'wmspiele'            => $wmspiele,
      'saisonliste_url'     => $saisonliste_url,
      'endspielliste'       => $endspielliste
    ]);
  }
  //
  public function worldchampionship_saison($input_saisonnr)
  {
    //
    $pokaldaten = [];
    //
    $maxSaisonNr = 0;
    $maxSzugPokalRunde = 0;
    //
    $steuerung = Steuerung::where('SNr', '=', 1)
      ->firstOrFail();
    //
    $maxSaisonNr = $steuerung->SzugSaisonNr;
    $maxSzugWMRunde = $steuerung->SzugWMRunde;
    // prüfe $input_saisonnr
    if ($input_saisonnr > $maxSaisonNr || $input_saisonnr < 0)
    {
      $fehlerkz = true;
      $fehlermeldung = 'Die vorgegebene Saison ist nicht gültig.';
      return view('pages.worldchampionship_saison', [
        'seitenname'      => 'cup_saison',
        'saisonnr'        => $input_saisonnr,
        'fehlerkz'        => $fehlerkz,
        'fehlermeldung'   => $fehlermeldung,
      ]);
    }
    // Berechne die gesamten Einnahmen der Teams
    $teameinnahmen_summe = DB::table('teamkonto')
       ->select('TeamName', DB::raw('SUM(TKWert) as Gesamteinnahme'))
       ->join('team', 'team.TeamNr', '=', 'teamkonto.TKzugTeamNr')
       ->where('TKzugSaisonNr', '=', $input_saisonnr)
       ->where('TKWert', '>', 0)
       ->groupBy('teamkonto.TKzugTeamNr')
       ->orderBy('Gesamteinnahme', 'DESC')
       ->orderBy('TeamName')
       ->get();
    //
    $pokaldaten[$input_saisonnr]['teameinnahmen_gesamt'] = '';
    $pokaldaten[$input_saisonnr]['teameinnahmen_gesamt'] = '<table class="table table-bordered table-striped">';
    //
    $pokaldaten[$input_saisonnr]['teameinnahmen_gesamt'] .= '<thead class="thead-dark">';
    $pokaldaten[$input_saisonnr]['teameinnahmen_gesamt'] .= '<tr>';
    $pokaldaten[$input_saisonnr]['teameinnahmen_gesamt'] .= '<th col"scope">Team</th>';
    $pokaldaten[$input_saisonnr]['teameinnahmen_gesamt'] .= '<th col"scope">Einnahme</th>';
    $pokaldaten[$input_saisonnr]['teameinnahmen_gesamt'] .= '</thead>';
    foreach($teameinnahmen_summe as $single_teameinnahmen_summe)
    {
       $pokaldaten[$input_saisonnr]['teameinnahmen_gesamt'] .= '<tr>';
         $pokaldaten[$input_saisonnr]['teameinnahmen_gesamt'] .= '<td>' . $single_teameinnahmen_summe->TeamName . '</td>';
         $pokaldaten[$input_saisonnr]['teameinnahmen_gesamt'] .= '<td>' . number_format($single_teameinnahmen_summe->Gesamteinnahme,2,",",".") . ' TCs</td>';
         $pokaldaten[$input_saisonnr]['teameinnahmen_gesamt'] .= '</tr>';
     }
     $pokaldaten[$input_saisonnr]['teameinnahmen_gesamt'] .= '</table>';
    // ermittle die Teameinnahmen aller Teams
    $teameinnahmen = DB::table('teamkonto')
      ->select('TeamName', 'TKWert', 'TKName')
      ->join('team', 'team.TeamNr', '=', 'teamkonto.TKzugTeamNr')
      ->where('TKzugSaisonNr', '=', $input_saisonnr)
      ->where('TKWert', '>', 0)
      ->orderBy('TeamNr')
      ->get();
    //
    $pokaldaten[$input_saisonnr]['teameinnahmen'] = '';
    $pokaldaten[$input_saisonnr]['teameinnahmen'] = '<table class="table table-bordered table-striped">';
    //
    $pokaldaten[$input_saisonnr]['teameinnahmen'] .= '<thead class="thead-dark">';
    $pokaldaten[$input_saisonnr]['teameinnahmen'] .= '<tr>';
    $pokaldaten[$input_saisonnr]['teameinnahmen'] .= '<th col"scope">Team</th>';
    $pokaldaten[$input_saisonnr]['teameinnahmen'] .= '<th col"scope">Einnahmetyp</th>';
    $pokaldaten[$input_saisonnr]['teameinnahmen'] .= '<th col"scope">Einnahme</th>';
    $pokaldaten[$input_saisonnr]['teameinnahmen'] .= '</thead>';
    foreach($teameinnahmen as $single_teameinnahme)
    {
        $pokaldaten[$input_saisonnr]['teameinnahmen'] .= '<tr>';
        $pokaldaten[$input_saisonnr]['teameinnahmen'] .= '<td>' . $single_teameinnahme->TeamName . '</td>';
        $pokaldaten[$input_saisonnr]['teameinnahmen'] .= '<td>' . $single_teameinnahme->TKName . '</td>';
        $pokaldaten[$input_saisonnr]['teameinnahmen'] .= '<td>' . number_format($single_teameinnahme->TKWert,2,",",".") . ' TCs</td>';
        $pokaldaten[$input_saisonnr]['teameinnahmen'] .= '</tr>';
    }
    $pokaldaten[$input_saisonnr]['teameinnahmen'] .= '</table>';
    // ermittle die WM-Torjägerliste
    $torjäger = DB::table('spieltore')
      ->select('PlayerName', 'PlayerTrikotNr', 'TeamName', DB::raw('COUNT(SpielToreNr) as Tore'))
      ->join('player', 'player.PlayerNr', '=', 'spieltore.SpielTorezugPlayerNr')
      ->join('team', 'team.TeamNr', '=', 'spieltore.SpielTorezugTeamNr')
      ->groupBy('SpielTorezugPlayerNr', 'PlayerName', 'PlayerTrikotNr', 'TeamName')
      ->where('SpielTorezugSaisonNr', '=', $input_saisonnr)
      ->where('SpielTorezugSpieltypNr', '>=', 200)
      ->where('SpielToreTyp', '=', 0)
      ->limit(50)
      ->orderBy('Tore', 'desc')
      ->orderBy('TeamName')
      ->get();
    //
    $pokaldaten[$input_saisonnr]['wmtorjägerliste'] = '';
    $pokaldaten[$input_saisonnr]['wmtorjägerliste'] = '<table class="table table-bordered table-striped">';
    //
    $pokaldaten[$input_saisonnr]['wmtorjägerliste'] .= '<thead class="thead-dark">';
    $pokaldaten[$input_saisonnr]['wmtorjägerliste'] .= '<tr>';
    $pokaldaten[$input_saisonnr]['wmtorjägerliste'] .= '<th col"scope">Torschütze</th>';
    $pokaldaten[$input_saisonnr]['wmtorjägerliste'] .= '<th col"scope">Verein</th>';
    $pokaldaten[$input_saisonnr]['wmtorjägerliste'] .= '<th col"scope">Tore</th>';
    $pokaldaten[$input_saisonnr]['wmtorjägerliste'] .= '</thead>';
    foreach($torjäger as $torschütze)
    {
        $pokaldaten[$input_saisonnr]['wmtorjägerliste'] .= '<tr>';
        $pokaldaten[$input_saisonnr]['wmtorjägerliste'] .= '<td>' . $torschütze->PlayerName . ' ('. $torschütze->PlayerTrikotNr . ')</td>';
        $pokaldaten[$input_saisonnr]['wmtorjägerliste'] .= '<td>' . $torschütze->TeamName . '</td>';
        $pokaldaten[$input_saisonnr]['wmtorjägerliste'] .= '<td>' . $torschütze->Tore . '</td>';
        $pokaldaten[$input_saisonnr]['wmtorjägerliste'] .= '</tr>';
    }
    $pokaldaten[$input_saisonnr]['wmtorjägerliste'] .= '</table>';
    // ermittle die Welt-Torjägerliste
    $torjäger = DB::table('spieltore')
      ->select('PlayerName', 'PlayerTrikotNr', 'TeamName', DB::raw('COUNT(SpielToreNr) as Tore'))
      ->join('player', 'player.PlayerNr', '=', 'spieltore.SpielTorezugPlayerNr')
      ->join('team', 'team.TeamNr', '=', 'spieltore.SpielTorezugTeamNr')
      ->groupBy('SpielTorezugPlayerNr', 'PlayerName', 'PlayerTrikotNr', 'TeamName')
      ->where('SpielTorezugSaisonNr', '=', $input_saisonnr)
      ->where('SpielToreTyp', '=', 0)
      ->limit(50)
      ->orderBy('Tore', 'desc')
      ->orderBy('TeamName')
      ->get();
    //
    $pokaldaten[$input_saisonnr]['welttorjägerliste'] = '';
    $pokaldaten[$input_saisonnr]['welttorjägerliste'] = '<table class="table table-bordered table-striped">';
    //
    $pokaldaten[$input_saisonnr]['welttorjägerliste'] .= '<thead class="thead-dark">';
    $pokaldaten[$input_saisonnr]['welttorjägerliste'] .= '<tr>';
    $pokaldaten[$input_saisonnr]['welttorjägerliste'] .= '<th col"scope">Torschütze</th>';
    $pokaldaten[$input_saisonnr]['welttorjägerliste'] .= '<th col"scope">Verein</th>';
    $pokaldaten[$input_saisonnr]['welttorjägerliste'] .= '<th col"scope">Tore</th>';
    $pokaldaten[$input_saisonnr]['welttorjägerliste'] .= '</thead>';
    foreach($torjäger as $torschütze)
    {
        $pokaldaten[$input_saisonnr]['welttorjägerliste'] .= '<tr>';
        $pokaldaten[$input_saisonnr]['welttorjägerliste'] .= '<td>' . $torschütze->PlayerName . ' ('. $torschütze->PlayerTrikotNr . ')</td>';
        $pokaldaten[$input_saisonnr]['welttorjägerliste'] .= '<td>' . $torschütze->TeamName . '</td>';
        $pokaldaten[$input_saisonnr]['welttorjägerliste'] .= '<td>' . $torschütze->Tore . '</td>';
        $pokaldaten[$input_saisonnr]['welttorjägerliste'] .= '</tr>';
    }
    $pokaldaten[$input_saisonnr]['welttorjägerliste'] .= '</table>';
    // WM-Runden
    for($saisonnr = $input_saisonnr; $saisonnr<=$input_saisonnr; $saisonnr++)
    {
      $pokaldaten[$saisonnr]['saisonnr'] = $saisonnr;
      //
      $pokaldaten[$saisonnr]['pokalrunden'] = '<div class="table-responsive-sm">';
      $pokaldaten[$saisonnr]['pokalrunden'] .= '<table class="table table-sm table-bordered table-striped">';
      //
      $pokaldaten[$saisonnr]['pokalrunden'] .= '<thead class="thead-dark">';
      $pokaldaten[$saisonnr]['pokalrunden'] .= '<tr>';
      $pokaldaten[$saisonnr]['pokalrunden'] .= '<th scope="col">Runde</th>';
      $pokaldaten[$saisonnr]['pokalrunden'] .= '<th scope="col">Hinspiel</th>';
      $pokaldaten[$saisonnr]['pokalrunden'] .= '<th scope="col">Rückspiel</th>';
      $pokaldaten[$saisonnr]['pokalrunden'] .= '</tr>';
      $pokaldaten[$saisonnr]['pokalrunden'] .= '</thead>';
      //
      $pokalrunde = 4;
      if ($maxSaisonNr == $saisonnr) {
        $pokalrunde = $maxSzugWMRunde;
      }
      //
      $pokaltabelle = WMTabelle::where('WMzugSaisonNr', '=', $saisonnr)
        ->where('WMRunde', '<=', $pokalrunde+1)
        ->orderBy('WMNr')
        ->get();
      //
      foreach($pokaltabelle as $spiel)
      {
        $pokaldaten[$saisonnr]['pokalrunden'] .= '<tr>';
        //
        $pokaldaten[$saisonnr]['pokalrunden'] .= '<td>' . ermittle_Pokalrundenname($spiel->WMRunde, 2) . '</td>';
        // Hinspiel
        $pokaldaten[$saisonnr]['pokalrunden'] .= '<td>';
        if ($spiel->WMHSpNr>0)
        {
          $pokaldaten[$saisonnr]['pokalrunden'] .= ermittle_Spielbericht($spiel->WMHSpNr);
          $pokaldaten[$saisonnr]['pokalrunden'] .= '<br />';
          $pokaldaten[$saisonnr]['pokalrunden'] .= ermittle_Teamnamen_Land($spiel->WMTeamANr);
          $pokaldaten[$saisonnr]['pokalrunden'] .= ' ' . $spiel->WMTAHTor . ' : ' . $spiel->WMTBHTor . ' ';
          $pokaldaten[$saisonnr]['pokalrunden'] .= ermittle_Teamnamen_Land($spiel->WMTeamBNr);
        } else {
          $pokaldaten[$saisonnr]['pokalrunden'] .= ermittle_Teamnamen_Land($spiel->WMTeamANr);
          $pokaldaten[$saisonnr]['pokalrunden'] .= ' : ';
          $pokaldaten[$saisonnr]['pokalrunden'] .= ermittle_Teamnamen_Land($spiel->WMTeamBNr);
        }
        $pokaldaten[$saisonnr]['pokalrunden'] .= '</td>';
        // Rückspiel
        $pokaldaten[$saisonnr]['pokalrunden'] .= '<td>';
        if ($spiel->WMRSpNr>0)
        {
          $pokaldaten[$saisonnr]['pokalrunden'] .= ermittle_Spielbericht($spiel->WMRSpNr);
          $pokaldaten[$saisonnr]['pokalrunden'] .= '<br />';
          $pokaldaten[$saisonnr]['pokalrunden'] .= ermittle_Teamnamen_Land($spiel->WMTeamBNr);
          $pokaldaten[$saisonnr]['pokalrunden'] .= ' ' . $spiel->WMTBRTor . ' : ' . $spiel->WMTARTor . ' ';
          $pokaldaten[$saisonnr]['pokalrunden'] .= ermittle_Teamnamen_Land($spiel->WMTeamANr);
        } else {
          $pokaldaten[$saisonnr]['pokalrunden'] .= ermittle_Teamnamen_Land($spiel->WMTeamBNr);
          $pokaldaten[$saisonnr]['pokalrunden'] .= ' : ';
          $pokaldaten[$saisonnr]['pokalrunden'] .= ermittle_Teamnamen_Land($spiel->WMTeamANr);
        }
        //
        $pokaldaten[$saisonnr]['pokalrunden'] .= '</tr>';
      }
      //
      $pokaldaten[$saisonnr]['pokalrunden'] .= '</table>';
      $pokaldaten[$saisonnr]['pokalrunden'] .= '</div>';
    }
    return view('pages.worldchampionship_saison', [
      'seitenname'      => 'worldchampionship_saison',
      'saisonnr'        => $input_saisonnr,
      'pokaldaten'      => $pokaldaten,
      'fehlerkz'        => false,
      'fehlermeldung'   => '',
    ]);
  }
  //
  public function cup()
  {
    // ermittle notwendige Werte aus der Tabelle Steuerung
    $maxSaisonNr = 0;
    //
    $steuerung = Steuerung::where('SNr', '=', 1)
      ->firstOrFail();
    //
    $maxSaisonNr = $steuerung->SzugSaisonNr;
    //
    $saisonliste = Saison::select('SaisonName', 'SaisonNr')
      ->where('SaisonNr', '<=', $maxSaisonNr)
      ->orderBy('SaisonNr')
      ->get();
    //
    $saisonliste_url = '';
    //
    foreach ($saisonliste as $single_saison) {
      $saisonliste_url .= '<a href="/cup_saison/' . $single_saison->SaisonNr . '" class="btn btn-success btn-block" role="button">' . $single_saison->SaisonName . '</a>';
    }
    // endspielliste
    $endspielliste = '<table class="table table-bordered table-striped">';
    $endspielliste .= '<thead class="thead-dark">';
    $endspielliste .= '<tr>';
    $endspielliste .= '<th scope="col">Saison</th>';
    $endspielliste .= '<th scope="col">Team A</th>';
    $endspielliste .= '<th scope="col">Team B</th>';
    $endspielliste .= '<th scope="col">Hinspiel</th>';
    $endspielliste .= '<th scope="col">Rückspiel</th>';
    $endspielliste .= '<th scope="col">Pokalsieger</th>';
    $endspielliste .= '</tr>';
    $endspielliste .= '</thead>';
    //
    $final_liste = DB::table('pokaltabelle')
      ->select('pokaltabelle.*', 'Heim.TeamName as HeimTeam', 'Gast.TeamName as GastTeam', 'Sieger.TeamName as SiegerTeam')
      ->join('team as Heim', 'Heim.TeamNr', '=', 'pokaltabelle.PTTeamANr')
      ->join('team as Gast', 'Gast.TeamNr', '=', 'pokaltabelle.PTTeamBNr')
      ->join('team as Sieger', 'Sieger.TeamNr', '=', 'pokaltabelle.PTTeamSiegerNr')
      ->where('PTRunde', '=', 8)
      ->orderBy('PTzugSaisonNr')
      ->get();
    //
    foreach($final_liste as $single_finale)
    {
      $endspielliste .= '<tr>';
      $endspielliste .= '<td>' . $single_finale->PTzugSaisonNr . '</td>';
      $endspielliste .= '<td>' . $single_finale->HeimTeam . '</td>';
      $endspielliste .= '<td>' . $single_finale->GastTeam . '</td>';
      $endspielliste .= '<td>' . $single_finale->PTTAHTor . ' : ' . $single_finale->PTTBHTor . '</td>';
      $endspielliste .= '<td>' . $single_finale->PTTARTor . ' : ' . $single_finale->PTTBRTor . '</td>';
      $endspielliste .= '<td>' . $single_finale->SiegerTeam . '</td>';
      $endspielliste .= '</tr>';
    }
    $endspielliste .= '</table>';
    //
    return view('pages.cup', [
      'seitenname'      => 'cup',
      'saisonliste_url' => $saisonliste_url,
      'endspielliste'   => $endspielliste
    ]);
  }
  //
  public function cup_saison($input_saisonnr)
  {
    //
    $pokaldaten = [];
    //
    $maxSaisonNr = 0;
    $maxSzugPokalRunde = 0;
    //
    $steuerung = Steuerung::where('SNr', '=', 1)
      ->firstOrFail();
    //
    $maxSaisonNr = $steuerung->SzugSaisonNr;
    $maxSzugPokalRunde = $steuerung->SzugPokalRunde;
    // prüfe $input_saisonnr
    if ($input_saisonnr > $maxSaisonNr || $input_saisonnr < 0)
    {
      $fehlerkz = true;
      $fehlermeldung = 'Die vorgegebene Saison ist nicht gültig.';
      return view('pages.cup_saison', [
        'seitenname'      => 'cup_saison',
        'saisonnr'        => $input_saisonnr,
        'fehlerkz'        => $fehlerkz,
        'fehlermeldung'   => $fehlermeldung,
      ]);
    }
    //
    for($saisonnr = $input_saisonnr; $saisonnr<=$input_saisonnr; $saisonnr++)
    {
      $pokaldaten[$saisonnr]['saisonnr'] = $saisonnr;
      //
      $pokaldaten[$saisonnr]['pokalrunden'] = '<div class="table-responsive-sm">';
      $pokaldaten[$saisonnr]['pokalrunden'] .= '<table class="table table-sm table-bordered table-striped">';
      //
      $pokaldaten[$saisonnr]['pokalrunden'] .= '<thead class="thead-dark">';
      $pokaldaten[$saisonnr]['pokalrunden'] .= '<tr>';
      $pokaldaten[$saisonnr]['pokalrunden'] .= '<th scope="col">Runde</th>';
      $pokaldaten[$saisonnr]['pokalrunden'] .= '<th scope="col">Hinspiel</th>';
      $pokaldaten[$saisonnr]['pokalrunden'] .= '<th scope="col">Rückspiel</th>';
      $pokaldaten[$saisonnr]['pokalrunden'] .= '<th scope="col">Sieger</th>';
      $pokaldaten[$saisonnr]['pokalrunden'] .= '</tr>';
      $pokaldaten[$saisonnr]['pokalrunden'] .= '</thead>';
      //
      $pokalrunde = 8;
      if ($maxSaisonNr == $saisonnr) {
        $pokalrunde = $maxSzugPokalRunde;
      }
      //
      $pokaltabelle = PokalTabelle::where('PTzugSaisonNr', '=', $saisonnr)
        ->where('PTRunde', '<=', $pokalrunde+1)
        ->orderBy('PTNr')
        ->get();
      //
      foreach($pokaltabelle as $spiel)
      {
        $pokaldaten[$saisonnr]['pokalrunden'] .= '<tr>';
        //
        $pokaldaten[$saisonnr]['pokalrunden'] .= '<td>' . ermittle_Pokalrundenname($spiel->PTRunde, 1) . '</td>';
        // Hinspiel
        $pokaldaten[$saisonnr]['pokalrunden'] .= '<td>';
        if ($spiel->PTHSpNr>0)
        {
          $pokaldaten[$saisonnr]['pokalrunden'] .= ermittle_Teamnamen($spiel->PTTeamANr);
          $pokaldaten[$saisonnr]['pokalrunden'] .= ' (' . ermittle_Teamnamen_Land($spiel->PTTeamANr) . ')';
          $pokaldaten[$saisonnr]['pokalrunden'] .= ' ' . $spiel->PTTAHTor . ' : ' . $spiel->PTTBHTor . ' ';
          $pokaldaten[$saisonnr]['pokalrunden'] .= ermittle_Teamnamen($spiel->PTTeamBNr);
          $pokaldaten[$saisonnr]['pokalrunden'] .= ' (' . ermittle_Teamnamen_Land($spiel->PTTeamBNr) . ')';
          $pokaldaten[$saisonnr]['pokalrunden'] .= '<br />';
          //
          $pokaldaten[$saisonnr]['pokalrunden'] .= '<a tabindex="0" class="badge badge-success" ';
          $pokaldaten[$saisonnr]['pokalrunden'] .= ' role="button" data-toggle="popover" ';
          $pokaldaten[$saisonnr]['pokalrunden'] .= ' data-html="true" data-placement="right" data-trigger="focus" title="Spielbericht" ';
          $pokaldaten[$saisonnr]['pokalrunden'] .= ' data-content="' . ermittle_Spielbericht($spiel->PTHSpNr) . '">zum Spielbericht</a>';
        } else {
          $pokaldaten[$saisonnr]['pokalrunden'] .= ermittle_Teamnamen($spiel->PTTeamANr);
          $pokaldaten[$saisonnr]['pokalrunden'] .= ' (' . ermittle_Teamnamen_Land($spiel->PTTeamANr) . ')';
          $pokaldaten[$saisonnr]['pokalrunden'] .= ' : ';
          $pokaldaten[$saisonnr]['pokalrunden'] .= ermittle_Teamnamen($spiel->PTTeamBNr);
          $pokaldaten[$saisonnr]['pokalrunden'] .= ' (' . ermittle_Teamnamen_Land($spiel->PTTeamBNr) . ')';
        }
        $pokaldaten[$saisonnr]['pokalrunden'] .= '</td>';
        // Rückspiel
        $pokaldaten[$saisonnr]['pokalrunden'] .= '<td>';
        if ($spiel->PTRSpNr>0)
        {
          $pokaldaten[$saisonnr]['pokalrunden'] .= ermittle_Teamnamen($spiel->PTTeamBNr);
          $pokaldaten[$saisonnr]['pokalrunden'] .= ' ' . $spiel->PTTBRTor . ' : ' . $spiel->PTTARTor . ' ';
          $pokaldaten[$saisonnr]['pokalrunden'] .= ermittle_Teamnamen($spiel->PTTeamANr);
          $pokaldaten[$saisonnr]['pokalrunden'] .= '<br />';
          //
          $pokaldaten[$saisonnr]['pokalrunden'] .= '<a tabindex="0" class="badge badge-success" ';
          $pokaldaten[$saisonnr]['pokalrunden'] .= ' role="button" data-toggle="popover" ';
          $pokaldaten[$saisonnr]['pokalrunden'] .= ' data-html="true" data-placement="left" data-trigger="focus" title="Spielbericht" ';
          $pokaldaten[$saisonnr]['pokalrunden'] .= ' data-content="' . ermittle_Spielbericht($spiel->PTRSpNr) . '">zum Spielbericht</a>';
          $pokaldaten[$saisonnr]['pokalrunden'] .= '<br />';
        } else {
          $pokaldaten[$saisonnr]['pokalrunden'] .= ermittle_Teamnamen($spiel->PTTeamBNr);
          $pokaldaten[$saisonnr]['pokalrunden'] .= ' : ';
          $pokaldaten[$saisonnr]['pokalrunden'] .= ermittle_Teamnamen($spiel->PTTeamANr);
        }
        // Sieger
        $pokaldaten[$saisonnr]['pokalrunden'] .= '<td>';
        if ($spiel->PTTeamSiegerNr>0)
        {
          $pokaldaten[$saisonnr]['pokalrunden'] .= ermittle_Teamnamen($spiel->PTTeamSiegerNr);
        }
        $pokaldaten[$saisonnr]['pokalrunden'] .= '</td>';
        //
        $pokaldaten[$saisonnr]['pokalrunden'] .= '</tr>';
      }
      //
      $pokaldaten[$saisonnr]['pokalrunden'] .= '</table>';
      $pokaldaten[$saisonnr]['pokalrunden'] .= '</div>';
    }
    return view('pages.cup_saison', [
      'seitenname'      => 'cup_saison',
      'saisonnr'        => $input_saisonnr,
      'pokaldaten'      => $pokaldaten,
      'fehlerkz'        => false,
      'fehlermeldung'   => '',
    ]);
  }
  //
  public function team_saison($liganr, $teamnr, $input_saisonnr)
  {
    //
    $saisondaten = [];
    // ermittle die Teamdaten (teamname, teaminfo)
    $single_team = Team::where('TeamNr', '=', $teamnr)
      ->first();
    //
    if ($single_team) {
      $teamname = $single_team->TeamName;
      //
      $teaminfo = 'Kontostand: ' . number_format($single_team->TeamKontostand,2,",",".") . '<br />';
      $teaminfo .= 'Teamwert: ' . number_format($single_team->TeamWert,2,",",".") . '<br />';
      $teaminfo .= 'Spieler 2: ' . $single_team->T02 . '<br />';
      $teaminfo .= 'Spieler 3: ' . $single_team->T03 . '<br />';
      $teaminfo .= 'Spieler 4: ' . $single_team->T04 . '<br />';
      $teaminfo .= 'Spieler 5: ' . $single_team->T05 . '<br />';
      $teaminfo .= 'Spieler 6: ' . $single_team->T06 . '<br />';
      $teaminfo .= 'Spieler 7: ' . $single_team->T07 . '<br />';
      $teaminfo .= 'Spieler 8: ' . $single_team->T08 . '<br />';
      $teaminfo .= 'Spieler 9: ' . $single_team->T09 . '<br />';
      $teaminfo .= 'Spieler 10: ' . $single_team->T10 . '<br />';
      $teaminfo .= 'Spieler 11: ' . $single_team->T11 . '<br />';
      $teaminfo .= 'Fitness-Summe: ' . ($single_team->T02 + $single_team->T03 + $single_team->T04 + $single_team->T05 + $single_team->T06 + $single_team->T07 + $single_team->T08 + $single_team->T09 + $single_team->T10 + $single_team->T11) . '<br />';
    } else {
      $fehlerkz = true;
      $fehlermeldung = 'Die vorgegebene Team-Nr ist nicht gültig.';
      return view('pages.team_saison', [
        'seitenname'      => 'team_saison',
        'saisonnr'        => $input_saisonnr,
        'liganr'          => $liganr,
        'liganame'        => $liganame,
        'teamnr'          => $teamnr,
        'teamname'        => '',
        'teaminfo'        => '',
        'mannschaft'      => [],
        'saisondaten'     => [],
        'fehlerkz'        => $fehlerkz,
        'fehlermeldung'   => $fehlermeldung,
      ]);
    }
    // ermittle Liganame
    $liganame = ermittle_Liganamen($liganr);
    //
    $maxSaisonNr = 0;
    $maxSzugSpieltagRunde = 0;
    //
    $steuerung = Steuerung::where('SNr', '=', 1)
      ->firstOrFail();
    //
    $maxSaisonNr = $steuerung->SzugSaisonNr;
    $maxSzugSpieltagRunde = $steuerung->SzugSpieltagRunde;
    // prüfe $input_saisonnr
    if ($input_saisonnr > $maxSaisonNr || $input_saisonnr < 0)
    {
      $fehlerkz = true;
      $fehlermeldung = 'Die vorgegebene Saison ist nicht gültig.';
      return view('pages.team_saison', [
        'seitenname'      => 'team_saison',
        'saisonnr'        => $input_saisonnr,
        'liganr'          => $liganr,
        'liganame'        => $liganame,
        'teamnr'          => $teamnr,
        'teamname'        => $teamname,
        'teaminfo'        => '',
        'mannschaft'      => [],
        'saisondaten'     => [],
        'fehlerkz'        => $fehlerkz,
        'fehlermeldung'   => $fehlermeldung,
      ]);
    }
    // ermittle die Namen der Mannschaft
    $mannschaft = '<table class="table table-bordered table-striped">';
    $mannschaft .= '<thead class="thead-dark">';
    $mannschaft .= '<tr>';
    $mannschaft .= '<th scope="col">Name</th>';
    $mannschaft .= '<th scope="col">Rückennummer</th>';
    $mannschaft .= '</tr>';
    $mannschaft .= '</thead>';
    //
    $mannschaft_liste = Player::where('PzugTeamNr', '=', $teamnr)->get();
    foreach($mannschaft_liste as $player)
    {
      $mannschaft .= '<tr>';
      $mannschaft .= '<td>' . $player->PlayerName . '</td>';
      $mannschaft .= '<td>' . $player->PlayerTrikotNr . '</td>';
      $mannschaft .= '</tr>';
    }
    $mannschaft .= '</table>';
    // ermittle die bisherigen Saisonspiele
    $spielliste = '<table class="table table-bordered table-striped">';
    $spielliste .= '<thead class="thead-dark">';
    $spielliste .= '<tr>';
    $spielliste .= '<th scope="col">Spiel</th>';
    $spielliste .= '<th scope="col">Heim</th>';
    $spielliste .= '<th scope="col">Ergebnis</th>';
    $spielliste .= '<th scope="col">Gast</th>';
    $spielliste .= '</tr>';
    $spielliste .= '</thead>';
    $spielteam_liste = DB::table('spielteam')
      ->select('spieltyp.SpielTypName as Spiel', 'Heim.TeamName as HeimTeam', 'Gast.TeamName as GastTeam', 'spiel.SpielHeimTore as HeimTore', 'spiel.SpielGastTore as GastTore')
      ->join('spiel', 'spiel.SpielNr', '=', 'spielteam.STzugSpielNr')
      ->join('spieltyp', 'spieltyp.SpieltypNr', '=', 'spiel.SpielTypNr')
      ->join('team as Heim', 'Heim.TeamNr', '=', 'spiel.SpielHeimTeamNr')
      ->join('team as Gast', 'Gast.TeamNr', '=', 'spiel.SpielGastTeamNr')
      ->where('STzugTeamNr', '=', $teamnr)
      ->where('STzugSaisonNr', '=', $input_saisonnr)
      ->get();
    //
    foreach($spielteam_liste as $single_spiel)
    {
      $spielliste .= '<tr>';
      $spielliste .= '<td>' . $single_spiel->Spiel . '</td>';
      $spielliste .= '<td>' . $single_spiel->HeimTeam . '</td>';
      $spielliste .= '<th>' . $single_spiel->HeimTore . ' : ' . $single_spiel->GastTore . '</th>';
      $spielliste .= '<td>' . $single_spiel->GastTeam . '</td>';
      $spielliste .= '</tr>';
    }
    $spielliste .= '</table>';
    //
    for($saisonnr = $input_saisonnr; $saisonnr<=$input_saisonnr; $saisonnr++)
    {
      $saisondaten[$saisonnr]['saisonnr'] = $saisonnr;
      // ====================
      // ermittle die Tabelle
      // ====================
      $saisondaten[$saisonnr]['saisontabelle'] = '<table width="100%" class="table table-bordered table-striped">';
      //
      $saisondaten[$saisonnr]['saisontabelle'] .= '<thead class="thead-dark">';
      $saisondaten[$saisonnr]['saisontabelle'] .= '<tr>';
      $saisondaten[$saisonnr]['saisontabelle'] .= '<th width="8%" scope="col">Platz</th>';
      $saisondaten[$saisonnr]['saisontabelle'] .= '<th width="41%" scope="col">Team</th>';
      $saisondaten[$saisonnr]['saisontabelle'] .= '<th width="9%" scope="col">Spieltag</th>';
      $saisondaten[$saisonnr]['saisontabelle'] .= '<th width="5%" scope="col">G</th>';
      $saisondaten[$saisonnr]['saisontabelle'] .= '<th width="5%" scope="col">U</th>';
      $saisondaten[$saisonnr]['saisontabelle'] .= '<th width="5%" scope="col">V</th>';
      $saisondaten[$saisonnr]['saisontabelle'] .= '<th width="9%" scope="col">Tore</th>';
      $saisondaten[$saisonnr]['saisontabelle'] .= '<th width="9%" scope="col">Differenz</th>';
      $saisondaten[$saisonnr]['saisontabelle'] .= '<th width="9%" scope="col">Punkte</th>';
      $saisondaten[$saisonnr]['saisontabelle'] .= '</tr>';
      $saisondaten[$saisonnr]['saisontabelle'] .= '</thead>';
      //
      $taballenspieltag = 30;
      if ($maxSaisonNr == $saisonnr) {
        $taballenspieltag = $maxSzugSpieltagRunde;
      }
      //
      $tabelle = DB::table('ligatabelle')
        ->select('ligatabelle.*', 'team.TeamName AS TeamName')
        ->join('team', 'team.TeamNr', '=', 'ligatabelle.LTzugTeamNr')
        ->where('LTzugSaisonNr', '=', $saisonnr)
        ->where('LTzugLandNr', '=', $liganr)
        ->where('LTSpieltag', '=', $taballenspieltag)
        ->orderBy('LTPlatz')
        ->orderBy('LTTorDifferenz', 'desc')
        ->orderBy('LTAnzahlSiege', 'desc')
        ->orderBy('LTPlusTore', 'desc')
        ->orderBy('LTzugTeamNr', 'desc')
        ->get();
      //
      foreach($tabelle as $platz)
      {
        if ($teamnr == $platz->LTzugTeamNr)
        {
          $saisondaten[$saisonnr]['saisontabelle'] .= '<tr class="table-success">';
        } else {
          $saisondaten[$saisonnr]['saisontabelle'] .= '<tr>';
        }

        $saisondaten[$saisonnr]['saisontabelle'] .= '<td>' . $platz->LTPlatz . '</td>';
        $saisondaten[$saisonnr]['saisontabelle'] .= '<td>' . $platz->TeamName . '</td>';
        $saisondaten[$saisonnr]['saisontabelle'] .= '<td>' . $platz->LTSpieltag . '</td>';
        $saisondaten[$saisonnr]['saisontabelle'] .= '<td>' . $platz->LTAnzahlSiege . '</td>';
        $saisondaten[$saisonnr]['saisontabelle'] .= '<td>' . $platz->LTAnzahlUnentschieden . '</td>';
        $saisondaten[$saisonnr]['saisontabelle'] .= '<td>' . $platz->LTAnzahlNiederlagen . '</td>';
        $saisondaten[$saisonnr]['saisontabelle'] .= '<td>' . $platz->LTPlusTore . ':'. $platz->LTMinusTore . '</td>';
        $saisondaten[$saisonnr]['saisontabelle'] .= '<td>' . $platz->LTTorDifferenz . '</td>';
        $saisondaten[$saisonnr]['saisontabelle'] .= '<td>' . $platz->LTPlusPunkte . '</td>';
        //
        $saisondaten[$saisonnr]['saisontabelle'] .= '</tr>';
      }
      //
      $saisondaten[$saisonnr]['saisontabelle'] .= '</table>';
      //
      $saisondaten[$saisonnr]['ligaheimspiele'] = '';
      //
      $ligaspiele_heim = Spiel::where('SpielzugSaisonNr', '=', $saisonnr)
        ->where('SpielHeimTeamNr', '=', $teamnr)
        ->where('SpielTypNr', '<=', 30)
        ->orderBy('SpielNr')
        ->get();
      //
      foreach($ligaspiele_heim as $single_spiel)
      {
        $saisondaten[$saisonnr]['ligaheimspiele'] .= '<div class="alert alert-success"';
        $saisondaten[$saisonnr]['ligaheimspiele'] .= $single_spiel->SpielBericht;
        $saisondaten[$saisonnr]['ligaheimspiele'] .= '</div>';
      }
      //
      $ligaspiele_gast = Spiel::where('SpielzugSaisonNr', '=', $saisonnr)
        ->where('SpielGastTeamNr', '=', $teamnr)
        ->where('SpielTypNr', '<=', 30)
        ->orderBy('SpielNr')
        ->get();
      //
      $saisondaten[$saisonnr]['ligagastspiele'] = '';
      //
      foreach($ligaspiele_gast as $single_spiel)
      {
        $saisondaten[$saisonnr]['ligagastspiele'] .= '<div class="alert alert-warning"';
        $saisondaten[$saisonnr]['ligagastspiele'] .= $single_spiel->SpielBericht;
        $saisondaten[$saisonnr]['ligagastspiele'] .= '</div>';
      }
      // ermittle die Torjägerliste
      $torjäger = DB::table('spieltore')
        ->select('PlayerName', 'PlayerTrikotNr', 'TeamName', DB::raw('COUNT(SpielToreNr) as Tore'))
        ->join('player', 'player.PlayerNr', '=', 'spieltore.SpielTorezugPlayerNr')
        ->join('team', 'team.TeamNr', '=', 'spieltore.SpielTorezugTeamNr')
        ->groupBy('SpielTorezugPlayerNr', 'PlayerName', 'PlayerTrikotNr', 'TeamName')
        ->where('SpielTorezugSaisonNr', '=', $saisonnr)
        ->where('SpielTorezugTeamNr', '=', $teamnr)
        ->where('SpielTorezugSpieltypNr', '<=', 30)
        ->where('SpielToreTyp', '=', 0)
        ->limit(15)
        ->orderBy('Tore', 'desc')
        ->orderBy('TeamName')
        ->get();
      //
      $saisondaten[$saisonnr]['ligatorjägerliste'] = '';
      $saisondaten[$saisonnr]['ligatorjägerliste'] = '<table class="table table-bordered table-striped">';
      //
      $saisondaten[$saisonnr]['ligatorjägerliste'] .= '<thead class="thead-dark">';
      $saisondaten[$saisonnr]['ligatorjägerliste'] .= '<tr>';
      $saisondaten[$saisonnr]['ligatorjägerliste'] .= '<th col"scope">Torschütze</th>';
      $saisondaten[$saisonnr]['ligatorjägerliste'] .= '<th col"scope">Verein</th>';
      $saisondaten[$saisonnr]['ligatorjägerliste'] .= '<th col"scope">Tore</th>';
      $saisondaten[$saisonnr]['ligatorjägerliste'] .= '</thead>';
      foreach($torjäger as $torschütze)
      {
          $saisondaten[$saisonnr]['ligatorjägerliste'] .= '<tr>';
          $saisondaten[$saisonnr]['ligatorjägerliste'] .= '<td>' . $torschütze->PlayerName . ' ('. $torschütze->PlayerTrikotNr . ')</td>';
          $saisondaten[$saisonnr]['ligatorjägerliste'] .= '<td>' . $torschütze->TeamName . '</td>';
          $saisondaten[$saisonnr]['ligatorjägerliste'] .= '<td>' . $torschütze->Tore . '</td>';
          $saisondaten[$saisonnr]['ligatorjägerliste'] .= '</tr>';
      }
      $saisondaten[$saisonnr]['ligatorjägerliste'] .= '</table>';
      //
      $saisondaten[$saisonnr]['pokalheimspiele'] = '';
      //
      $pokalspiele_heim = Spiel::where('SpielzugSaisonNr', '=', $saisonnr)
        ->where('SpielHeimTeamNr', '=', $teamnr)
        ->whereBetween('SpielTypNr', [100, 120])
        ->orderBy('SpielNr')
        ->get();
      //
      foreach($pokalspiele_heim as $single_spiel)
      {
        $saisondaten[$saisonnr]['pokalheimspiele'] .= '<div class="alert alert-success"';
        $saisondaten[$saisonnr]['pokalheimspiele'] .= $single_spiel->SpielBericht;
        $saisondaten[$saisonnr]['pokalheimspiele'] .= '</div>';
      }
      //
      $pokalspiele_gast = Spiel::where('SpielzugSaisonNr', '=', $saisonnr)
        ->where('SpielGastTeamNr', '=', $teamnr)
        ->whereBetween('SpielTypNr', [100, 120])
        ->orderBy('SpielNr')
        ->get();
      //
      $saisondaten[$saisonnr]['pokalgastspiele'] = '';
      //
      foreach($pokalspiele_gast as $single_spiel)
      {
        $saisondaten[$saisonnr]['pokalgastspiele'] .= '<div class="alert alert-warning"';
        $saisondaten[$saisonnr]['pokalgastspiele'] .= $single_spiel->SpielBericht;
        $saisondaten[$saisonnr]['pokalgastspiele'] .= '</div>';
      }
      // ermittle die Torjägerliste
      $torjäger = DB::table('spieltore')
        ->select('PlayerName', 'PlayerTrikotNr', 'TeamName', DB::raw('COUNT(SpielToreNr) as Tore'))
        ->join('player', 'player.PlayerNr', '=', 'spieltore.SpielTorezugPlayerNr')
        ->join('team', 'team.TeamNr', '=', 'spieltore.SpielTorezugTeamNr')
        ->groupBy('SpielTorezugPlayerNr', 'PlayerName', 'PlayerTrikotNr', 'TeamName')
        ->where('SpielTorezugSaisonNr', '=', $saisonnr)
        ->where('SpielTorezugTeamNr', '=', $teamnr)
        ->whereBetween('SpielTorezugSpieltypNr', [100, 120])
        ->where('SpielToreTyp', '=', 0)
        ->limit(15)
        ->orderBy('Tore', 'desc')
        ->orderBy('TeamName')
        ->get();
      //
      $saisondaten[$saisonnr]['pokaltorjägerliste'] = '';
      $saisondaten[$saisonnr]['pokaltorjägerliste'] = '<table class="table table-bordered table-striped">';
      //
      $saisondaten[$saisonnr]['pokaltorjägerliste'] .= '<thead class="thead-dark">';
      $saisondaten[$saisonnr]['pokaltorjägerliste'] .= '<tr>';
      $saisondaten[$saisonnr]['pokaltorjägerliste'] .= '<th col"scope">Torschütze</th>';
      $saisondaten[$saisonnr]['pokaltorjägerliste'] .= '<th col"scope">Verein</th>';
      $saisondaten[$saisonnr]['pokaltorjägerliste'] .= '<th col"scope">Tore</th>';
      $saisondaten[$saisonnr]['pokaltorjägerliste'] .= '</thead>';
      foreach($torjäger as $torschütze)
      {
          $saisondaten[$saisonnr]['pokaltorjägerliste'] .= '<tr>';
          $saisondaten[$saisonnr]['pokaltorjägerliste'] .= '<td>' . $torschütze->PlayerName . ' ('. $torschütze->PlayerTrikotNr . ')</td>';
          $saisondaten[$saisonnr]['pokaltorjägerliste'] .= '<td>' . $torschütze->TeamName . '</td>';
          $saisondaten[$saisonnr]['pokaltorjägerliste'] .= '<td>' . $torschütze->Tore . '</td>';
          $saisondaten[$saisonnr]['pokaltorjägerliste'] .= '</tr>';
      }
      $saisondaten[$saisonnr]['pokaltorjägerliste'] .= '</table>';
    }
    //
    return view('pages.team_saison', [
      'seitenname'      => 'team_saison',
      'saisonnr'        => $input_saisonnr,
      'liganr'          => $liganr,
      'liganame'        => $liganame,
      'teamnr'          => $teamnr,
      'teamname'        => $teamname,
      'teaminfo'        => $teaminfo,
      'mannschaft'      => $mannschaft,
      'spielliste'      => $spielliste,
      'saisondaten'     => $saisondaten,
      'fehlerkz'        => false,
      'fehlermeldung'   => '',
    ]);
  }
  //
  public function team($liganr, $teamnr)
  {
    // ermittle Teamname
    $teamname = ermittle_Teamnamen($teamnr);
    // ermittle Liganame
    $liganame = ermittle_Liganamen($liganr);
    // ermittle notwendige Werte aus der Tabelle Steuerung
    $maxSaisonNr = 0;
    //
    $steuerung = Steuerung::where('SNr', '=', 1)
      ->firstOrFail();
    //
    $maxSaisonNr = $steuerung->SzugSaisonNr;
    //
    $saisonliste = Saison::select('SaisonName', 'SaisonNr')
      ->where('SaisonNr', '<=', $maxSaisonNr)
      ->orderBy('SaisonNr')
      ->get();
    //
    $saisonliste_url = '';
    //
    foreach ($saisonliste as $single_saison) {
      $saisonliste_url .= '<a href="/team_saison/' . $liganr . '/'. $teamnr . '/' . $single_saison->SaisonNr . '" class="btn btn-success" role="button">' . $single_saison->SaisonName . '</a>';
    }
    // ermittle die bisherigen Platzierungen
    $platzierungen = '<table class="table table-bordered table-striped">';
    $platzierungen .= '<thead class="thead-dark">';
    $platzierungen .= '<tr>';
    $platzierungen .= '<th scope="col">Saison</th>';
    $platzierungen .= '<th scope="col">Platz</th>';
    $platzierungen .= '<th scope="col">Punkte</th>';
    $platzierungen .= '<th scope="col">S</th>';
    $platzierungen .= '<th scope="col">U</th>';
    $platzierungen .= '<th scope="col">N</th>';
    $platzierungen .= '<th scope="col">Tore</th>';
    $platzierungen .= '<th scope="col">Gegentore</th>';
    $platzierungen .= '<th scope="col">Tordifferenz</th>';
    $platzierungen .= '</tr>';
    $platzierungen .= '</thead>';
    //
    $platzierung_liste = DB::table('ligatabelle')
      ->select('ligatabelle.*')
      ->where('LTzugTeamNr', '=', $teamnr)
      ->where('LTSpieltag', '=', 30)
      ->orderBy('LTzugSaisonNr')
      ->get();
    //
    foreach($platzierung_liste as $single_platzierung)
    {
      $platzierungen .= '<tr>';
      $platzierungen .= '<td>' . $single_platzierung->LTzugSaisonNr . '</td>';
      $platzierungen .= '<td>' . $single_platzierung->LTPlatz . '</td>';
      $platzierungen .= '<td>' . $single_platzierung->LTPlusPunkte . '</td>';
      $platzierungen .= '<td>' . $single_platzierung->LTAnzahlSiege . '</td>';
      $platzierungen .= '<td>' . $single_platzierung->LTAnzahlUnentschieden . '</td>';
      $platzierungen .= '<td>' . $single_platzierung->LTAnzahlNiederlagen . '</td>';
      $platzierungen .= '<td>' . $single_platzierung->LTPlusTore . '</td>';
      $platzierungen .= '<td>' . $single_platzierung->LTMinusTore . '</td>';
      $platzierungen .= '<td>' . $single_platzierung->LTTorDifferenz . '</td>';
      $platzierungen .= '</tr>';
    }
    $platzierungen .= '</table>';
    // ermittle die bisherigen Saisonspiele
    $spielliste = '<table class="table table-bordered table-striped">';
    $spielliste .= '<thead class="thead-dark">';
    $spielliste .= '<tr>';
    $spielliste .= '<th scope="col">Spiel</th>';
    $spielliste .= '<th scope="col">Saison</th>';
    $spielliste .= '<th scope="col">Heim</th>';
    $spielliste .= '<th scope="col">Ergebnis</th>';
    $spielliste .= '<th scope="col">Gast</th>';
    $spielliste .= '<th scope="col">eigene Werte (TW,SW)</th>';
    $spielliste .= '<th scope="col">Gegner-Werte (TW,SW)</th>';
    $spielliste .= '</tr>';
    $spielliste .= '</thead>';
    $spielteam_liste = DB::table('spielteam')
      ->select('spieltyp.SpielTypName as Spiel',
        'spielteam.STTeamwert as STTeamwert',
        'spielteam.STSpielwert as STSpielwert',
        'spielteam.STGegnerTeamwert as STGegnerTeamwert',
        'spielteam.STGegnerSpielwert as STGegnerSpielwert',
        'Heim.teamname as HeimTeam',
        'Gast.teamname as GastTeam',
        'spiel.SpielHeimTore as HeimTore',
        'spiel.SpielGastTore as GastTore',
        'spiel.SpielzugSaisonNr as Saison')
      ->join('spiel', 'spiel.SpielNr', '=', 'spielteam.STzugSpielNr')
      ->join('spieltyp', 'spieltyp.SpieltypNr', '=', 'spiel.SpielTypNr')
      ->join('team as Heim', 'Heim.TeamNr', '=', 'spiel.SpielHeimTeamNr')
      ->join('team as Gast', 'Gast.TeamNr', '=', 'spiel.SpielGastTeamNr')
      ->where('STzugTeamNr', '=', $teamnr)
      ->orderBy('spiel.SpielNr', 'DESC')
      ->get();
    //
    foreach($spielteam_liste as $single_spiel)
    {
      $spielliste .= '<tr>';
      $spielliste .= '<td>' . $single_spiel->Spiel . '</td>';
      $spielliste .= '<td>' . $single_spiel->Saison . '</td>';
      $spielliste .= '<td>' . $single_spiel->HeimTeam . '</td>';
      $spielliste .= '<td>' . $single_spiel->HeimTore . ' : ' . $single_spiel->GastTore . '</td>';
      $spielliste .= '<td>' . $single_spiel->GastTeam . '</td>';
      $spielliste .= '<td>' . $single_spiel->STTeamwert . ' - ' . $single_spiel->STSpielwert . '</td>';
      $spielliste .= '<td>' . $single_spiel->STGegnerTeamwert . ' - ' . $single_spiel->STGegnerSpielwert . '</td>';
      $spielliste .= '</tr>';
    }
    $spielliste .= '</table>';
    //
    return view('pages.team', [
      'seitenname'      => 'team',
      'liganr'          => $liganr,
      'liganame'        => $liganame,
      'teamname'        => $teamname,
      'saisonliste_url' => $saisonliste_url,
      'platzierungen'   => $platzierungen,
      'spielliste'      => $spielliste,
    ]);
  }
  //
  public function saison($liganr, $saisonnr)
  {
    // ermittle Liganame
    $liganame = ermittle_Liganamen($liganr);
    // ermittle Saisonname
    $saisonname = ermittle_Saisonnamen($saisonnr);
    //
    $maxSaisonNr = 0;
    $maxSzugSpieltagRunde = 0;
    //
    $steuerung = Steuerung::where('SNr', '=', 1)
      ->firstOrFail();
    //
    $maxSaisonNr = $steuerung->SzugSaisonNr;
    $maxSzugSpieltagRunde = $steuerung->SzugSpieltagRunde;
    //
    $spieltagliste_url = '';
    // =========================
    // Fall 1: Saison läuft noch
    // =========================
    if ($saisonnr == $maxSaisonNr)
    {
      for($spieltag = 1; $spieltag <= 30; $spieltag++)
      {
        if ($spieltag <= $maxSzugSpieltagRunde)
        {
          $spieltagliste_url .= '<a href="/spieltag/' . $liganr . '/'. $saisonnr . '/' . $spieltag . '" class="btn btn-success" role="button">' . $spieltag . '. Spieltag</a>';
        } else {
          $spieltagliste_url .= '<a href="/spieltag/' . $liganr . '/'. $saisonnr . '/' . $spieltag . '" class="btn btn-warning" role="button">' . $spieltag . '. Spieltag</a>';
        }
      }
    }
    // =======================================
    // Fall 2:Saison ist bereits abgeschlossen
    // =======================================
    if ($saisonnr < $maxSaisonNr)
    {
      for($spieltag = 1; $spieltag <= 30; $spieltag++)
      {
        $spieltagliste_url .= '<a href="/spieltag/' . $liganr . '/'. $saisonnr . '/' . $spieltag . '" class="btn btn-success" role="button">' . $spieltag . '. Spieltag</a>';
      }
    }
    //
    return view('pages.saison', [
      'seitenname'        => 'saison',
      'liganr'            => $liganr,
      'liganame'          => $liganame,
      'saisonname'        => $saisonname,
      'spieltagliste_url' => $spieltagliste_url,
    ]);
  }
  public function spieltag($liganr, $saisonnr, $spieltag)
  {
    // ermittle Liganame
    $liganame = ermittle_Liganamen($liganr);
    // ermittle Saisonname
    $saisonname = ermittle_Saisonnamen($saisonnr);
    //
    $maxSaisonNr = 0;
    $maxSzugSpieltagRunde = 0;
    //
    $steuerung = Steuerung::where('SNr', '=', 1)
      ->firstOrFail();
    //
    $maxSaisonNr = $steuerung->SzugSaisonNr;
    $maxSzugSpieltagRunde = $steuerung->SzugSpieltagRunde;
    // ermittle, ob Spieltag schon gespielt wurde
    $spieltag_gespielt_kz = false;
    //
    if ($maxSaisonNr > $saisonnr)
    {
      $spieltag_gespielt_kz = true;
    }
    if ($maxSaisonNr == $saisonnr)
    {
      if ($maxSzugSpieltagRunde >= $spieltag)
      {
        $spieltag_gespielt_kz = true;
      }
    }
        $spieltagdaten[$spieltag]['torjägerliste'] = '';
    //
    $spieltagname = $spieltag . '.ter Spieltag';
    //
    $spielberichte = '';
    if ($spieltag_gespielt_kz == false)
    {
      $spielberichte = 'Es liegen noch keine Spielberichte vor, da dieser Spieltag noch nicht gespielt wurde.';
    }
    //
    $spielpaarungen = '';
    $spieltagtabelle = 'Es liegt noch keine Tabelle vor, da dieser Spieltag noch nicht gespielt wurde.';
    $torjägerliste = 'Es liegt noch keine Torjägerliste vor, da dieser Spieltag noch nicht gespielt wurde.';
    // ermittle Spielpaarungen
    if ($maxSaisonNr == $saisonnr) {
      // ermittle $liste_spielplan mit Hilfe der Tabelle SpielPlan
      if ($spieltag < 16) {
        $liste_spielplan = SpielPlan::where('SpieltagNr', '=', $spieltag)
          ->get();
      } else {
        $spieltag2 = $spieltag - 15;
        $liste_spielplan = SpielPlan::where('SpieltagNr', '=', $spieltag2)
          ->get();
      }
    } else {
      // ermittle $liste_spielplan mit Hilfe der Tabelle Spiel
      $liste_spielplan = DB::table('spiel')
        ->select('spiel.SpielHeimTeamnr AS HeimTeamNr', 'spiel.SpielGastTeamnr AS GastTeamNr')
        ->where('spiel.SpielzugLandNr', '=', $liganr)
        ->where('spiel.SpielzugSaisonNr', '=', $saisonnr)
        ->where('spiel.SpielTypNr', '=', $spieltag)
        ->get();
    }
    //
    $spielpaarungen = '<table class="table table-bordered table-striped">';
    $spielpaarungen .= '<thead class="thead-dark">';
    $spielpaarungen .= '<tr>';
    $spielpaarungen .= '<th col"scope">Heim</th>';
    $spielpaarungen .= '<th col"scope" class="text-center">Ergebnis</th>';
    $spielpaarungen .= '<th col"scope">Gast</th>';
    $spielpaarungen .= '</thead>';
    //
    foreach($liste_spielplan as $single_spielplan) {
      if ($maxSaisonNr == $saisonnr) {
        if ($spieltag < 16) {
          $heimnr = $single_spielplan->HeimNr;
          $heimname = ermittle_Teamnamen_Spielplan($liganr, $heimnr);
          $heimteamnr = ermittle_TeamNr_Spielplan($liganr, $heimnr);
          //
          $gastnr = $single_spielplan->GastNr;
          $gastname = ermittle_Teamnamen_Spielplan($liganr, $gastnr);
          $gastteamnr = ermittle_TeamNr_Spielplan($liganr, $gastnr);
        } else
        {
          $heimnr = $single_spielplan->GastNr;
          $heimname = ermittle_Teamnamen_Spielplan($liganr, $heimnr);
          $heimteamnr = ermittle_TeamNr_Spielplan($liganr, $heimnr);
          //
          $gastnr = $single_spielplan->HeimNr;
          $gastname = ermittle_Teamnamen_Spielplan($liganr, $gastnr);
          $gastteamnr = ermittle_TeamNr_Spielplan($liganr, $gastnr);
        }
      } else {
        $heimname = ermittle_Teamnamen($single_spielplan->HeimTeamNr);
        $heimteamnr = $single_spielplan->HeimTeamNr;
        //
        $gastname = ermittle_Teamnamen($single_spielplan->GastTeamNr);
        $gastteamnr = $single_spielplan->GastTeamNr;
      }
      //
      $spiel = ermittle_Spiel($liganr, $saisonnr, $spieltag, $heimteamnr, $gastteamnr);
      //
      $heimtore = '&nbsp;';
      $gasttore = '&nbsp;';
      // Prüfe, ob Spiel vorhanden ist
      if ($spiel)
      {
        $heimtore = $spiel->SpielHeimTore;
        $gasttore = $spiel->SpielGastTore;
        //
        $spielberichte .= $spiel->SpielBericht;
        $spielberichte .= '<hr/>';
      }
      //
      $spielpaarungen .= '<tr>';
      //
      $spielpaarungen .= '<td>';
      $spielpaarungen .= $heimname;
      $spielpaarungen .= '</td>';
      $spielpaarungen .= '<th class="text-center">';
      $spielpaarungen .= $heimtore . ':' . $gasttore;
      $spielpaarungen .= '</th>';
      $spielpaarungen .= '<td>';
      $spielpaarungen .= $gastname;
      $spielpaarungen .= '</td>';
      //
      $spielpaarungen .= '</tr>';
    }
    $spielpaarungen .= '</table>';
    //
    if ($spieltag_gespielt_kz)
    {
      // ermittle die Tabelle
      $spieltagtabelle= '<table width="100%" class="table table-bordered table-striped">';
      //
      $spieltagtabelle.= '<thead class="thead-dark">';
      $spieltagtabelle.= '<tr>';
      $spieltagtabelle.= '<th width="8%" scope="col">Platz</th>';
      $spieltagtabelle.= '<th width="41%" scope="col">Team</th>';
      $spieltagtabelle.= '<th width="9%" scope="col">Spieltag</th>';
      $spieltagtabelle.= '<th width="5%" scope="col">G</th>';
      $spieltagtabelle.= '<th width="5%" scope="col">U</th>';
      $spieltagtabelle.= '<th width="5%" scope="col">V</th>';
      $spieltagtabelle.= '<th width="9%" scope="col">Tore</th>';
      $spieltagtabelle.= '<th width="9%" scope="col">Differenz</th>';
      $spieltagtabelle.= '<th width="9%" scope="col">Punkte</th>';
      $spieltagtabelle.= '</tr>';
      $spieltagtabelle.= '</thead>';
      //
      $tabelle = DB::table('ligatabelle')
        ->select('ligatabelle.*', 'team.TeamName AS TeamName')
        ->join('team', 'team.TeamNr', '=', 'ligatabelle.LTzugTeamNr')
        ->where('LTzugSaisonNr', '=', $saisonnr)
        ->where('LTzugLandNr', '=', $liganr)
        ->where('LTSpieltag', '=', $spieltag)
        ->orderBy('LTPlatz')
        ->orderBy('LTTorDifferenz', 'desc')
        ->orderBy('LTAnzahlSiege', 'desc')
        ->orderBy('LTPlusTore', 'desc')
        ->orderBy('LTzugTeamNr', 'desc')
        ->get();
      //
      foreach($tabelle as $platz)
      {
        $spieltagtabelle.= '<tr>';
        $spieltagtabelle.= '<td>' . $platz->LTPlatz . '</td>';
        $spieltagtabelle.= '<td>' . $platz->TeamName . '</td>';
        $spieltagtabelle.= '<td>' . $platz->LTSpieltag . '</td>';
        $spieltagtabelle.= '<td>' . $platz->LTAnzahlSiege . '</td>';
        $spieltagtabelle.= '<td>' . $platz->LTAnzahlUnentschieden . '</td>';
        $spieltagtabelle.= '<td>' . $platz->LTAnzahlNiederlagen . '</td>';
        $spieltagtabelle.= '<td>' . $platz->LTPlusTore . ':'. $platz->LTMinusTore . '</td>';
        $spieltagtabelle.= '<td>' . $platz->LTTorDifferenz . '</td>';
        $spieltagtabelle.= '<td>' . $platz->LTPlusPunkte . '</td>';
        $spieltagtabelle.= '</tr>';
      }
      //
      $spieltagtabelle.= '</table>';
      // ermittle die Torjägerliste
      $torjäger = DB::table('spieltore')
        ->select('PlayerName', 'PlayerTrikotNr', 'TeamName', DB::raw('COUNT(SpielToreNr) as Tore'))
        ->join('player', 'player.PlayerNr', '=', 'spieltore.SpielTorezugPlayerNr')
        ->join('team', 'team.TeamNr', '=', 'spieltore.SpielTorezugTeamNr')
        ->groupBy('SpielTorezugPlayerNr', 'PlayerName', 'PlayerTrikotNr', 'TeamName')
        ->where('SpielTorezugSaisonNr', '=', $saisonnr)
        ->where('SpielTorezugLandNr', '=', $liganr)
        ->where('SpielTorezugSpieltypNr', '<=', $spieltag)
        ->where('SpielToreTyp', '=', 0)
        ->limit(15)
        ->orderBy('Tore', 'desc')
        ->orderBy('TeamName')
        ->get();
      //
      $torjägerliste = '<table class="table table-bordered table-striped">';
      //
      $torjägerliste .= '<thead class="thead-dark">';
      $torjägerliste .= '<tr>';
      $torjägerliste .= '<th col"scope">Torschütze</th>';
      $torjägerliste .= '<th col"scope">Verein</th>';
      $torjägerliste .= '<th col"scope">Tore</th>';
      $torjägerliste .= '</thead>';
      foreach($torjäger as $torschütze)
      {
          $torjägerliste .= '<tr>';
          $torjägerliste .= '<td>' . $torschütze->PlayerName . ' ('. $torschütze->PlayerTrikotNr . ')</td>';
          $torjägerliste .= '<td>' . $torschütze->TeamName . '</td>';
          $torjägerliste .= '<td>' . $torschütze->Tore . '</td>';
          $torjägerliste .= '</tr>';
      }
      $torjägerliste .= '</table>';
    }
    //
    return view('pages.spieltag', [
      'seitenname'      => 'spieltag',
      'liganr'          => $liganr,
      'liganame'        => $liganame,
      'saisonnr'        => $saisonnr,
      'saisonname'      => $saisonname,
      'spieltag'        => $spieltag,
      '$spieltagname'   => $spieltagname,
      'spielberichte'   => $spielberichte,
      'spielpaarungen'  => $spielpaarungen,
      'spieltagtabelle' => $spieltagtabelle,
      'torjägerliste'   => $torjägerliste,
    ]);
  }
  //
  public function liga($liganr)
  {
    // ermittle Liganame
    $liganame = ermittle_Liganamen($liganr);
    // ermittle notwendige Werte aus der Tabelle Steuerung
    $maxSaisonNr = 0;
    //
    $steuerung = Steuerung::where('SNr', '=', 1)
      ->firstOrFail();
    //
    $maxSaisonNr = $steuerung->SzugSaisonNr;
    //
    $saisonliste = Saison::select('SaisonName', 'SaisonNr')
      ->where('SaisonNr', '<=', $maxSaisonNr)
      ->orderBy('SaisonNr')
      ->get();
    //
    $saisonliste_url = '';
    //
    foreach ($saisonliste as $single_saison) {
      $landesmeister = 'wird noch ermittelt';
      if ($single_saison->SaisonNr < $maxSaisonNr)
      {
        $landesmeister = ermittle_Landesmeister_Saison($single_saison->SaisonNr, $liganr);
      }
      //
      $saisonliste_url .= '<a href="/saison/' . $liganr . '/' . $single_saison->SaisonNr . '">' . $single_saison->SaisonName . ' (' . $landesmeister .')</a><br />';
    }
    //
    $teamliste = Team::select('TeamName', 'TeamNr', 'TeamWert')
      ->where('TzugLandNr', '=', $liganr)
      ->orderBy('TeamName')
      ->get();
    //
    $teamliste_url = '';
    //
    foreach ($teamliste as $single_team) {
      $teamliste_url .= '<a href="/team/' . $liganr . '/' . $single_team->TeamNr . '">' . $single_team->TeamName .  ' (' . $single_team->TeamWert  . ')</a><br />';
    }
    //
    return view('pages.leagues', [
      'seitenname'      => 'liga',
      'liganr'          => $liganr,
      'liganame'        => $liganame,
      'saisonliste_url' => $saisonliste_url,
      'teamliste_url'   => $teamliste_url,
    ]);
  }
  //
  public function country()
  {
    $ligen = Land::all();
    //
    return view('pages.country', [
      'seitenname'      => 'ligen',
      'ligen'           => $ligen
    ]);
  }
}
